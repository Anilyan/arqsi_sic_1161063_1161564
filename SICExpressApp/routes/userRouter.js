var express = require('express');
var router = express.Router();
var User = require('../app/models/user');

// on routes that end in /user
// ----------------------------------------------------
router.route('/')
    // get all users (accessed at GET http://localhost:8080/api/user)
    .get(function (req, res) {
        User.find(function (err, users) {
            if (err)
                res.send(err);
            
            res.json(users);
        });
    })
    .post(function (req, res) {

        var user = new User();
        user.name = req.body.name;
        user.password = req.body.password;
        user.email = req.body.email;
        user.isClient = req.body.isClient;

        // save and check for errors
        user.save(function (err) {
            if (err)
                res.send(err);

            res.json(user);
        });

    });

router.route('/:id')
    // get User with given id (accessed at GET http://localhost:8080/api/user/:id)
    .get(function (req, res) {
        User.findById(req.params.id, function (err, user) {
            if (err)
                res.send(err);
            
            res.json(user);
        });
    })

    // update User with given id (accessed at PUT http://localhost:8080/api/user/:id)
    .put(function (req, res) {

        // use our user model to find the wanted user
        User.findById(req.params.id, function (err, user) {

            if (err)
                res.send(err);
            if(req.body.name!=null){
                user.nome = req.body.name;                
            }
            if(req.body.password!=null){
                user.password = req.body.password;                
            }
            if(req.body.email!=null){
                user.email = req.body.email;                
            } 
            if(req.body.isClient!=null){
                user.isClient = req.body.isClient;               
            }         
            
            // save user
            user.save(function (err) {
                if (err)
                    res.send(err);

                res.json(user);
            });

        });
    })

    // delete user with given id (accessed at DELETE http://localhost:8080/api/user/:id)
    .delete(function (req, res) {
        User.remove({
            _id: req.params.id
        }, function (err, u) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });


module.exports = router;
