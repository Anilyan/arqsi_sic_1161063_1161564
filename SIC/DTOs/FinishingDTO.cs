using System.Collections.Generic;
using SIC.Models;

namespace SIC.DTOs
{
    public class FinishingDTO
    {
        public string Designation { get; set; }
    }
}