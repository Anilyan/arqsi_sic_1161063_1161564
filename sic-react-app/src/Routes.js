import React from 'react';
import {HomeView, Login, Signup, Catalog, Customize, Cart, NotFound} from './modules';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import history from './modules/history';


export default () => (
    <Router>
        <Switch>
            <Route exact path='/' />
            {/* <Route path='/login' component={Login} />
            <Route path='/signup' component={Signup} />
            <Route path='/catalog' component={Catalog} />
            <Route path='/customize' component={Customize} />
            <Route path='/cart' component={Cart} /> */}
            <Route path='*' component={NotFound} />
        </Switch>
    </Router>
);
