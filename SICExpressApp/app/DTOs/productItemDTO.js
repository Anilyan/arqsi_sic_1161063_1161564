module.exports = {
    productItemDTO : function(ProductItem) {
    return {
        name: ProductItem.name,
        product: ProductItem.product,
        concreteHeight: ProductItem.concreteDimensions.height,
        concreteWidth: ProductItem.concreteDimensions.width,
        concreteDepth: ProductItem.concreteDimensions.depth,
        parent: ProductItem.parent,
        // parts: ProductItem.parts,
        cost: ProductItem.cost
      }
  }
}