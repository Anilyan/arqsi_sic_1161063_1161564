using System.Collections.Generic;
using SIC.Models;

namespace SIC.DTOs
{
    public class CategoryDTO
    {
        public long Id { get; set; }
        
        public string Name { get; set; }
        
        public long? ParentCategoryId { get; set; }
        public IList<long> SubCategories { get; set; }
    }
}