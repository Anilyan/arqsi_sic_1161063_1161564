import React from 'react';
import img from '../../resources/404.png'

const NotFound = () => (
    <div>
        <center><img src={img}/></center>
    </div>
);

export default NotFound;