using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIC.DAL;
using SIC.DTOs;
using SIC.Models;

namespace SIC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FinishingController : ControllerBase
    {
        private readonly BusinessContext _context;

        public FinishingController(BusinessContext context)
        {
            _context = context;
//            if(!_context.Finishings.Any()){
//                Finishing sampleFinishing = new Finishing{Designation = new Name{NameContent = "sampleFinishing"}};
//                _context.Finishings.Add(sampleFinishing);
//                _context.SaveChanges();
//            }
        }

        // GET: api/Finishing
        [HttpGet]
        public IEnumerable<FinishingDTO> GetFinishings()
        {
            var finishingList = from f in _context.Finishings
                .Include(f => f.Designation)
             select f.toDTO();
            return finishingList;
        }

        // GET: api/Finishing/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFinishing([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Finishing finishing = GetFinishingFromId(id);

            if (finishing == null)
            {
                return NotFound();
            }

            return Ok(finishing.toDTO());
        }
        
        /**
         * Returns the finishing within the database with the specified id
         * Null if there is no finishing with this id
         */
        private Finishing GetFinishingFromId(long id)
        {
            
            IEnumerable<Finishing> finishings = _context.Finishings
                .Include(f => f.Designation);

            Finishing finishingToReturn = null;
            foreach (var finishing in finishings)
            {
                if (finishing.Id.Equals(id))
                {
                    finishingToReturn = finishing;
                    break;
                }
            }

            return finishingToReturn;
        }

        // PUT: api/Finishing/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFinishing([FromRoute] long id, [FromBody] Finishing finishing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Finishing finishingToChange = GetFinishingFromId(id);

            finishingToChange.Designation = finishing.Designation;
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FinishingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Finishing
        [HttpPost]
        public async Task<IActionResult> PostFinishing([FromBody] Finishing finishing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Finishings.Add(finishing);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFinishing", new { id = finishing.Id }, finishing.toDTO());
        }

        // DELETE: api/Finishing/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFinishing([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Finishing finishing = GetFinishingFromId(id);
            
            if (finishing == null)
            {
                return NotFound();
            }

            _context.Finishings.Remove(finishing);
            await _context.SaveChangesAsync();

            FinishingDTO finishingDTO = finishing.toDTO();
            return Ok(finishingDTO);
        }

        private bool FinishingExists(long id)
        {
            return _context.Finishings.Any(e => e.Id == id);
        }
    }
}