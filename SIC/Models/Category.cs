using System.Collections.Generic;
using System.Linq;
using SIC.DTOs;

namespace SIC.Models
{
    public class Category
    {
        public long Id { get; set; }
        public Name Name { get; set; }
        
        //tree structure
        public long? ParentCategoryId { get; set; }
        public Category ParentCategory { get; set; }
        public IList<Category> SubCategories { get; set; }

        /**
        Converts an instance of Category to its DTO equivalent
         */
        public CategoryDTO toDTO() {
            var category = new CategoryDTO(){
                Id = Id,
                Name =  Name.NameContent,
                ParentCategoryId = ParentCategoryId,
                SubCategories = SubCategories.Count != 0 ? 
                    SubCategories.Select(sc => sc.Id).ToList() 
                    : new List<long>()
            };

            return category;
        }
    }
}