using System.Linq;

namespace SIC.Models.Restrictions
{
    public class RestrictionOccupation : IRestriction<Product>
    {
        public bool ValidateAggregation(Product parentProduct, Product partToBeAdded)
        {
            
            // parent maximum dimensions
            var parentHeight = parentProduct.Dimensions.Height.Discrete
                ? parentProduct.Dimensions.Height.DiscreteValues.Max().ValueDouble
                : parentProduct.Dimensions.Height.ContinuousValues.Max().ValueDouble;
            var parentWidth = parentProduct.Dimensions.Width.Discrete
                ? parentProduct.Dimensions.Width.DiscreteValues.Max().ValueDouble
                : parentProduct.Dimensions.Width.ContinuousValues.Max().ValueDouble;
            var parentDepth = parentProduct.Dimensions.Depth.Discrete
                ? parentProduct.Dimensions.Depth.DiscreteValues.Max().ValueDouble
                : parentProduct.Dimensions.Depth.ContinuousValues.Max().ValueDouble;
            
            // part minimum dimensions
            var partHeight = partToBeAdded.Dimensions.Height.Discrete
                ? partToBeAdded.Dimensions.Height.DiscreteValues.Min().ValueDouble
                : partToBeAdded.Dimensions.Height.ContinuousValues.Min().ValueDouble;
            var partWidth = partToBeAdded.Dimensions.Width.Discrete
                ? partToBeAdded.Dimensions.Width.DiscreteValues.Min().ValueDouble
                : partToBeAdded.Dimensions.Width.ContinuousValues.Min().ValueDouble;
            var partDepth = partToBeAdded.Dimensions.Depth.Discrete
                ? partToBeAdded.Dimensions.Depth.DiscreteValues.Min().ValueDouble
                : partToBeAdded.Dimensions.Depth.ContinuousValues.Min().ValueDouble;

            var heightRatio = partHeight / parentHeight;
            var widthRatio = partWidth / parentWidth;
            var depthRatio = partDepth / parentDepth;

            var totalRatio = (heightRatio + widthRatio + depthRatio) / 3;

            return totalRatio > partToBeAdded.MinOccupation && totalRatio < partToBeAdded.MaxOccupation;
        }
    }
}