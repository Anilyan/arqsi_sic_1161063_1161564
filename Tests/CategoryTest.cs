using System;
using Xunit;
using SIC.Models;
using SIC.DTOs;
using System.Collections.Generic;

namespace Tests
{
    public class CategoryTest
    {
        
        private Category _category;

        public CategoryTest()
        {
            
        }
        
        [Fact]
        public void testToDto()
        {
            _category = new Category{ Name = new Name{ NameContent = "sampleName" } };

            var _categoryDTO = _category.toDTO();
            
            Assert.True(_categoryDTO.Name.Equals("sampleName"), "");
        }
    }
}
