var idvalidator = require('mongoose-id-validator');
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

/**
 * This ProductItem has the following attributes from the AbstractProduct:
 * - Name equal to the AbstractProduct name
 * - Category equal to the AbstractProduct category
 * - Materials (list) equal to the AbstractProduct materials
 * - Dimensions that fit
 * - Parts that fit in the dimensions and exist
 */

var ProductItemSchema   = new Schema({
    name: String,
    product: Number, //selected product id
    concreteDimensions: {
        height: Number,
        width: Number,
        depth: Number,
        },
    parent: {type: mongoose.Schema.Types.ObjectId, ref: 'ProductItem'},
    parts: [{type: mongoose.Schema.Types.ObjectId, ref: 'ProductItem'}],
    cost: Number
});

ProductItemSchema.plugin(idvalidator);
// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('ProductItem', ProductItemSchema);
