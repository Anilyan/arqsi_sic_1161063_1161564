using System;
using Xunit;
using SIC.Models;
using SIC.DTOs;
using System.Collections.Generic;

namespace Tests
{
    public class MaterialTest
    {
        
        private Material _material;

        public MaterialTest()
        {
            
        }
        
        [Fact]
        public void testToDto()
        {
            _material = new Material{Designation = new Name{NameContent = "sampleMaterial"},
                Finishing = new Finishing{Designation = new Name{NameContent = "sampleFinishing"}}};

            var _materialDTO = _material.toDTO();
            
            Assert.True(_materialDTO.Designation.Equals("sampleMaterial"), "");
            Assert.True(_materialDTO.Finishing.Designation.Equals("sampleFinishing"), "");
        }
    }
}
