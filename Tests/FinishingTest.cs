using System;
using Xunit;
using SIC.Models;
using SIC.DTOs;
using System.Collections.Generic;

namespace Tests
{
    public class FinishingTest
    {
        
        private Finishing _finishing;

        public FinishingTest()
        {
            
        }
        
        [Fact]
        public void testToDto()
        {
            _finishing = new Finishing{Designation = new Name{NameContent = "sampleFinishing"}};

            var _finishingDTO = _finishing.toDTO();
            
            Assert.True(_finishingDTO.Designation.Equals("sampleFinishing"), "");
        }
    }
}
