var mongoose            = require('mongoose');
var Schema              = mongoose.Schema;

var UserSchema   = new Schema({
    name: String,
    password: String,
    email: String,
    isClient: Boolean,
});

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('User', UserSchema);