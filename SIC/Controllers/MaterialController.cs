using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIC.DAL;
using SIC.DTOs;
using SIC.Models;

namespace SIC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaterialController : ControllerBase
    {
        private readonly BusinessContext _context;

        public MaterialController(BusinessContext context)
        {
            _context = context;
//            if(!_context.Categories.Any()){
//                Category sampleCategory= new Category{Name = new Name{NameContent = "sampleCategory"}, SubCategories = {}};
//                _context.Categories.Add(sampleCategory);
//                _context.SaveChanges();
//            }
        }

        // GET: api/Material
        [HttpGet]
        public IEnumerable<MaterialDTO> GetMaterials()
        {
            var materials = from m in _context.Materials
                    .Include(m => m.Designation)
                    .Include(m => m.FinishingList).ThenInclude(fl => fl.Designation)
            select m.ToDTO();
            return materials;
        }

        // GET: api/Material/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMaterial([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Material material = GetMaterialFromId(id);

            if (material == null)
            {
                return NotFound();
            }
            return Ok(material.ToDTO());
        }
        
        // GET: api/Material/Finishing/5
        [HttpGet("Finishing/{id}")]
        public async Task<IActionResult> GetMaterialAndFinishing([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var material = GetMaterialFromId(id);

            if (material == null)
            {
                return NotFound();
            }
            return Ok(material.FinishingList.Select(fl => fl.toDTO()).ToList());
        }
        
        /**
         * Returns the material within the database with the specified id
         * Null if there is no material with this id
         */
        private Material GetMaterialFromId(long id)
        {
            
            IEnumerable<Material> materials = _context.Materials
                .Include(m => m.Designation)
                .Include(m => m.FinishingList).ThenInclude(f => f.Designation);

            Material materialToReturn = null;
            foreach (var material in materials)
            {
                if (material.Id.Equals(id))
                {
                    materialToReturn = material;
                    break;
                }
            }

            return materialToReturn;
        }

        // PUT: api/Material/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMaterial([FromRoute] long id, [FromBody] Material material)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Material materialToChange = GetMaterialFromId(id);

            materialToChange.Designation = material.Designation;
            materialToChange.FinishingList = material.FinishingList;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaterialExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Material
        [HttpPost]
        public async Task<IActionResult> PostMaterial([FromBody] Material material)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Materials.Add(material);
            await _context.SaveChangesAsync();
            
            return CreatedAtAction("GetMaterial", new { id = material.Id }, material.ToDTO());
        }

        // DELETE: api/Material/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMaterial([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Material material = GetMaterialFromId(id);
            
            if (material == null)
            {
                return NotFound();
            }

            _context.Entry(material).State = EntityState.Deleted;
            await _context.SaveChangesAsync();

            MaterialDTO materialDTO = material.ToDTO();
            return Ok(materialDTO);
        }

        private bool MaterialExists(long id)
        {
            return _context.Materials.Any(e => e.Id == id);
        }
    }
}