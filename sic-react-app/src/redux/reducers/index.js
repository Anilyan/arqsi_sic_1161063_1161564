import { combineReducers } from 'redux';
import catalogFetchReducer from './catalog/fetchReducer';
import catalogCreateReducer from './catalog/createReducer';
import catalogDeleteReducer from './catalog/deleteReducer';

export default combineReducers({
    products: catalogFetchReducer,
    catalogCreate: catalogCreateReducer,
    catalogDelete: catalogDeleteReducer
});

