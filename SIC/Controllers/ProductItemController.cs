//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.EntityFrameworkCore;
//using SIC.DAL;
//using SIC.DTOs;
//using SIC.Models;
//
//namespace SIC.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class ProductItemController : ControllerBase
//    {
//        private readonly BusinessContext _context;
//
//        public ProductItemController(BusinessContext context)
//        {
//            _context = context;
//        }
//        
//        /**
//         * Returns the product item within the database with the specified id
//         * Null if there is no product item with this id
//         */
//        private ProductItem GetProductItemFromId(long id)
//        {
//            
//            IEnumerable<ProductItem> productItems = _context.ProductItem
//                .Include(pi => pi.AbstractProduct)
//                .Include(pi => pi.ConcreteDimensions)
//                .Include(pi => pi.Parts);
//
//            ProductItem productItemToReturn = null;
//            foreach (var productItem in productItems)
//            {
//                if (productItem.Id.Equals(id))
//                {
//                    productItemToReturn = productItem;
//                    break;
//                }
//            }
//
//            return productItemToReturn;
//        }
//
//        // GET: api/ProductItem
//        [HttpGet]
//        public IEnumerable<ProductItemDTO> GetProductItem()
//        {
//            IEnumerable<ProductItem> productItems = _context.ProductItem
//                .Include(pi => pi.AbstractProduct)
//                .Include(pi => pi.ConcreteDimensions)
//                .Include(pi => pi.Parts);
//            
//            return productItems.Select(pi => pi.toDTO());
//        }
//
//        // GET: api/ProductItem/5
//        [HttpGet("{id}")]
//        public async Task<IActionResult> GetProductItem([FromRoute] long id)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            var productItem = GetProductItemFromId(id);
//
//            if (productItem == null)
//            {
//                return NotFound();
//            }
//
//            return Ok(productItem.toDTO());
//        }
//
//        // PUT: api/ProductItem/5
//        [HttpPut("{id}")]
//        public async Task<IActionResult> PutProductItem([FromRoute] long id, [FromBody] ProductItem productItem)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            var productItemToChange = GetProductItemFromId(id);
//
//            productItemToChange.AbstractProduct = productItem.AbstractProduct;
//            productItemToChange.ConcreteDimensions = productItem.ConcreteDimensions;
//            productItemToChange.Parts = productItem.Parts;
//
//            _context.Entry(productItemToChange).State = EntityState.Modified;
//
//            try
//            {
//                await _context.SaveChangesAsync();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!ProductItemExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }
//
//            return NoContent();
//        }
//
//        // TODO
//        // POST: api/ProductItem
//        [HttpPost]
//        public async Task<IActionResult> PostProductItem([FromBody] ProductItem productItem)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            _context.ProductItem.Add(productItem);
//            await _context.SaveChangesAsync();
//
//            return CreatedAtAction("GetProductItem", new { id = productItem.Id }, productItem);
//        }
//
//        // TODO
//        // DELETE: api/ProductItem/5
//        [HttpDelete("{id}")]
//        public async Task<IActionResult> DeleteProductItem([FromRoute] long id)
//        {
//            if (!ModelState.IsValid)
//            {
//                return BadRequest(ModelState);
//            }
//
//            var productItem = await _context.ProductItem.FindAsync(id);
//            if (productItem == null)
//            {
//                return NotFound();
//            }
//
//            _context.ProductItem.Remove(productItem);
//            await _context.SaveChangesAsync();
//
//            return Ok(productItem);
//        }
//
//        private bool ProductItemExists(long id)
//        {
//            return _context.ProductItem.Any(e => e.Id == id);
//        }
//    }
//}