
import React from 'react'
import { Button, Form, Dropdown, TextArea } from 'semantic-ui-react'
// import { countryOptions } from '../common'
const options = [
  { key: 'angular', text: 'Angular', value: 'angular' },
  { key: 'css', text: 'CSS', value: 'css' },
  { key: 'design', text: 'Graphic Design', value: 'design' },
  { key: 'ember', text: 'Ember', value: 'ember' },
  { key: 'html', text: 'HTML', value: 'html' },
  { key: 'ia', text: 'Information Architecture', value: 'ia' },
  { key: 'javascript', text: 'Javascript', value: 'javascript' },
  { key: 'mech', text: 'Mechanical Engineering', value: 'mech' },
  { key: 'meteor', text: 'Meteor', value: 'meteor' },
  { key: 'node', text: 'NodeJS', value: 'node' },
  { key: 'plumbing', text: 'Plumbing', value: 'plumbing' },
  { key: 'python', text: 'Python', value: 'python' },
  { key: 'rails', text: 'Rails', value: 'rails' },
  { key: 'react', text: 'React', value: 'react' },
  { key: 'repair', text: 'Kitchen Repair', value: 'repair' },
  { key: 'ruby', text: 'Ruby', value: 'ruby' },
  { key: 'ui', text: 'UI Design', value: 'ui' },
  { key: 'ux', text: 'User Experience', value: 'ux' },
];


const Find = () => (
  <div class="Product-data">
    <Form>
      <div class="Product-dimensions">
        <Form.Group widths='equal'>
          <Dropdown placeholder='Select Product Item' fluid search selection options={options} />
          <Button type='submit'>Submit</Button>
        </Form.Group>
      </div>
      <TextArea autoHeight readOnly placeholder='Product Item data' />
    </Form>
  </div>
);

export default Find;
