import React from 'react'
import { Button, Checkbox, Form, Dropdown } from 'semantic-ui-react'
// import { countryOptions } from '../common'
const options = [
  { key: 'angular', text: 'Angular', value: 'angular' },
  { key: 'css', text: 'CSS', value: 'css' },
  { key: 'design', text: 'Graphic Design', value: 'design' },
  { key: 'ember', text: 'Ember', value: 'ember' },
  { key: 'html', text: 'HTML', value: 'html' },
  { key: 'ia', text: 'Information Architecture', value: 'ia' },
  { key: 'javascript', text: 'Javascript', value: 'javascript' },
  { key: 'mech', text: 'Mechanical Engineering', value: 'mech' },
  { key: 'meteor', text: 'Meteor', value: 'meteor' },
  { key: 'node', text: 'NodeJS', value: 'node' },
  { key: 'plumbing', text: 'Plumbing', value: 'plumbing' },
  { key: 'python', text: 'Python', value: 'python' },
  { key: 'rails', text: 'Rails', value: 'rails' },
  { key: 'react', text: 'React', value: 'react' },
  { key: 'repair', text: 'Kitchen Repair', value: 'repair' },
  { key: 'ruby', text: 'Ruby', value: 'ruby' },
  { key: 'ui', text: 'UI Design', value: 'ui' },
  { key: 'ux', text: 'User Experience', value: 'ux' },
];


const Create = () => (
  <div class="Product-data">
    <Form>
    <Form.Group widths='equal'>
    <Form.Field>
      <label>Product Item Name</label>
      <input placeholder='Product Name' />
    </Form.Field>
      <Form.Field>
        <label>Product selected</label>
        <Dropdown placeholder='Select Product' fluid search selection options={options} />
      </Form.Field>
    </Form.Group>
    <Form.Group widths='equal'>
        <Form.Field>
            <Dropdown placeholder='Specify width' fluid search selection options={options} />
        </Form.Field>
        <Form.Field>
            <Dropdown placeholder='Specify height' fluid search selection options={options} />
        </Form.Field>
        <Form.Field>
            <Dropdown placeholder='Specify depth' fluid search selection options={options} />
        </Form.Field>
    </Form.Group>
    <Form.Group widths='equal'>
      <Form.Field>
        <Dropdown placeholder='Select Parent' fluid search selection options={options} />
      </Form.Field>
      <Form.Field>
        <Dropdown placeholder='Select Parts' fluid multiple selection options={options} />
      </Form.Field>
    </Form.Group>
    <Button type='submit'>Submit</Button>
  </Form>
  </div>
);

export default Create;