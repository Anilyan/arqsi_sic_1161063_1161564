using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIC.DAL;
using SIC.DTOs;
using SIC.Models;

namespace SIC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly BusinessContext _context;

        public ProductController(BusinessContext context)
        {
            _context = context;
             if(!_context.Products.Any()){
//                 var sampleName = new Name{NameContent = "sampleName"};
//                 var sampleCategory= new Category{Name = new Name{NameContent = "sampleCategory"}, 
//                     SubCategories = {}};
//                 var sampleMaterials = new List<Material>{new Material{Designation = new Name{NameContent = "sampleMaterial"},
//                     FinishingList = new List<Finishing>{new Finishing{Designation = new Name{NameContent = "sampleFinishing"}}}}};
//                 var values1 = new List<Value> {new Value {ValueDouble = 20.0}};
//                 var values2 = new List<Value> {new Value {ValueDouble = 20.0}};
//                 var values3 = new List<Value> {new Value {ValueDouble = 20.0}};
//                 var sampleDimensions = new Dimensions {
//                     Height = new Dimension { Discrete = true, DiscreteValues = values1, ContinuousValues = null },
//                     Width = new Dimension { Discrete = true, DiscreteValues = values2, ContinuousValues = null },
//                     Depth = new Dimension { Discrete = true, DiscreteValues = values3, ContinuousValues = null }
//                 };
//                 var product = new Product{Name = sampleName, Category = sampleCategory, Materials = sampleMaterials, 
//                     Dimensions = sampleDimensions, Parts = new List<Product>()};
//                 _context.Products.Add(product);
//                 _context.SaveChanges();

//                 var parts = new List<Product> {product};
//
//                 _context.Products.Add(new Product
//                 {
//                     Name = sampleName, 
//                     Category = sampleCategory,
//                     Materials = sampleMaterials,
//                     Dimensions = sampleDimensions,
//                     Parts = parts
//                 });
//                 _context.SaveChanges();
            }
        }

        // GET: api/Product
        [HttpGet]
        public IEnumerable<ProductDTO> GetProducts()
        {
            var products = _context.Products
                .Include(p => p.Name)
                .Include(p => p.Category).ThenInclude(c => c.Name)
                .Include(p => p.Category).ThenInclude(c => c.ParentCategory)
                .Include(p => p.Category).ThenInclude(c => c.SubCategories).ThenInclude(sc => sc.Name)
                .Include(p => p.Dimensions).ThenInclude(d => d.Depth).ThenInclude(d => d.ContinuousValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Depth).ThenInclude(d => d.DiscreteValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Width).ThenInclude(d => d.ContinuousValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Width).ThenInclude(d => d.DiscreteValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Height).ThenInclude(d => d.ContinuousValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Height).ThenInclude(d => d.DiscreteValues)
                .Include(p => p.Materials).ThenInclude(m => m.Designation)
                .Include(p => p.Materials).ThenInclude(m => m.FinishingList).ThenInclude(f => f.Designation)
                .Include(p => p.Parts);

            return products.Select(p => p.toDTO());
        }
        
        // GET: api/Product/?name=product1
        [HttpGet("name={name}")]
        public async Task<IActionResult> GetProductGivingName([FromRoute] string name)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = GetProductFromName(name);

            if (product == null)
            {
                return NotFound();
            }
            
            return Ok(product.toDTO());
        }

        // GET: api/Product/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduct([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = GetProductFromId(id);

            if (product == null)
            {
                return NotFound();
            }
            
            return Ok(product.toDTO());
        }
        
        // GET: api/Product/5/parts
        [HttpGet("{id}/parts")]
        public IEnumerable<ProductDTO> GetProductParts([FromRoute] long id)
        {
            
            var product = GetProductFromId(id);

            var productParts = product.Parts;

            return productParts.Select(productPart => productPart.toDTO()).ToList();
        }
        
        // GET: api/Product/5/partIn
        [HttpGet("{id}/partIn")]
        public IEnumerable<ProductDTO> GetProductPartIn([FromRoute] long id)
        {
            
            Product product = GetProductFromId(id);
            var productPartInDto = new List<ProductDTO>();

            var existingProducts = _context.Products;

            foreach (var existingProduct in existingProducts)
            {
                var existingProductParts = existingProduct.Parts;
                foreach (var existingProductPart in existingProductParts)
                {
                    if (existingProductPart.Equals(product))
                    {
                        productPartInDto.Add(existingProduct.toDTO());
                        break;
                    }
                }
            }

            return productPartInDto;
        }
        
        /**
         * Returns the product within the database with the specified id
         * Null if there is no product with this id
         */
        private Product GetProductFromId(long id)
        {
            
            IEnumerable<Product> products = _context.Products
                .Include(p => p.Name)
                .Include(p => p.Category).ThenInclude(c => c.Name)
                .Include(p => p.Category).ThenInclude(c => c.ParentCategory)
                .Include(p => p.Category).ThenInclude(c => c.SubCategories).ThenInclude(sc => sc.Name)
                .Include(p => p.Dimensions).ThenInclude(d => d.Depth).ThenInclude(d => d.ContinuousValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Depth).ThenInclude(d => d.DiscreteValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Width).ThenInclude(d => d.ContinuousValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Width).ThenInclude(d => d.DiscreteValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Height).ThenInclude(d => d.ContinuousValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Height).ThenInclude(d => d.DiscreteValues)
                .Include(p => p.Materials).ThenInclude(m => m.Designation)
                .Include(p => p.Materials).ThenInclude(m => m.FinishingList).ThenInclude(f => f.Designation)
                .Include(p => p.Parts);

            Product productToReturn = null;
            foreach (var product in products)
            {
                if (product.Id.Equals(id))
                {
                    productToReturn = product;
                    break;
                }
            }

            return productToReturn;
        }
        
        /**
         * Returns the product within the database with the specified name
         * Null if there is no product with this name
         */
        private Product GetProductFromName(string name)
        {
            
            IEnumerable<Product> products = _context.Products
                .Include(p => p.Name)
                .Include(p => p.Category).ThenInclude(c => c.Name)
                .Include(p => p.Category).ThenInclude(c => c.ParentCategory)
                .Include(p => p.Category).ThenInclude(c => c.SubCategories).ThenInclude(sc => sc.Name)
                .Include(p => p.Dimensions).ThenInclude(d => d.Depth).ThenInclude(d => d.ContinuousValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Depth).ThenInclude(d => d.DiscreteValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Width).ThenInclude(d => d.ContinuousValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Width).ThenInclude(d => d.DiscreteValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Height).ThenInclude(d => d.ContinuousValues)
                .Include(p => p.Dimensions).ThenInclude(d => d.Height).ThenInclude(d => d.DiscreteValues)
                .Include(p => p.Materials).ThenInclude(m => m.Designation)
                .Include(p => p.Materials).ThenInclude(m => m.FinishingList).ThenInclude(f => f.Designation)
                .Include(p => p.Parts);

            Product productToReturn = null;
            foreach (var product in products)
            {
                if (product.Name.NameContent.Equals(name))
                {
                    productToReturn = product;
                    break;
                }
            }

            return productToReturn;
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct([FromRoute] long id, [FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productToChange = GetProductFromId(id);
            
            if (product.Category != null)
            {
                var category = new CategoryController(_context).GetCategoryFromId(product.Category.Id);
                if (category == null)
                {
                    return NotFound("Category with id " + product.Category.Id + " was not found\nProduct not created");
                }

                productToChange.Category = category;
            }
            
            // if the parent product id is the same, nothing about this is changed
            if (product.ParentProductId != productToChange.ParentProductId)
            {
                Product parentProduct = null;
                if (product.ParentProductId != null)
                {
                    // if the parent product to be added doesn't exist, the process stops
                    parentProduct = GetProductFromId((long) product.ParentProductId);
                    if (parentProduct == null)
                    {
                        return NotFound("Product with id " + product.ParentProductId + " was not found\nProduct not updated");
                    }

                    if (!parentProduct.AddPart(productToChange))
                    {
                        return BadRequest("This product doesn't fit in the parent product's parts\nProduct not updated");
                    }
                }
                
                if (productToChange.ParentProductId != null)
                {
                    // remove this product from the sub categories of the parent product
                    productToChange.ParentProduct.Parts.Remove(product);
                    productToChange.ParentProduct = null;
                }
                
                productToChange.ParentProductId = product.ParentProductId;
                productToChange.ParentProduct = parentProduct;
            }
            
            if (product.Name != null)
            {
                productToChange.Name = product.Name;
            }
            if (product.Dimensions != null)
            {
                productToChange.Dimensions = product.Dimensions;
            }
            if (product.Materials != null)
            {
                productToChange.Materials = product.Materials;
            }

            var auxiliaryList = new List<Product>(productToChange.Parts);

            // deletes the parts in the product that are not to be kept
            foreach (var productToChangePart in auxiliaryList)
            {
                var keepPart = false;
                foreach (var productPart in product.Parts)
                {
                    if (productPart.Id == productToChangePart.Id)
                    {
                        keepPart = true;
                        break;
                    }
                }

                if (!keepPart)
                {
                    await DeleteProduct(productToChangePart.Id);
                }
            }
            
            // add the new sub categories
            foreach (var part in product.Parts)
            {
                var partFromId = GetProductFromId(part.Id);
                
                // if the sub product doesn't exist, it isn't added to the product
                if (partFromId != null && !productToChange.Parts.Contains(partFromId))
                {
                    if (productToChange.AddPart(partFromId))
                    {
                        partFromId.ParentProduct = productToChange;
                        partFromId.ParentProductId = id;
                    }
                }
            }
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetProduct", new { id = productToChange.Id }, productToChange.toDTO());
        }

        // POST: api/Product
        [HttpPost]
        public async Task<IActionResult> PostProduct([FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var category = new CategoryController(_context).GetCategoryFromId(product.Category.Id);
            if (category == null)
            {
                return NotFound("Category with id " + product.Category.Id + " was not found\nProduct not created");
            }

            long? parentProductId = null;
            Product parentProduct = null;
            if (product.ParentProductId != null)
            {
                parentProduct = GetProductFromId((long) product.ParentProductId);
                if (parentProduct == null)
                {
                    return NotFound("Product with id " + product.ParentProductId + " was not found\nProduct not created");
                }
                parentProductId = product.ParentProductId;
            }

            //a product without parts
            var dummyProduct = new Product
            {
                Name = product.Name,
                Category = category,
                Dimensions = product.Dimensions,
                Materials = product.Materials,
                ParentProduct = parentProduct,
                ParentProductId = parentProductId,
                Parts = new List<Product>()
            };
            
            _context.Products.Add(dummyProduct);
            await _context.SaveChangesAsync();

            if (dummyProduct.ParentProduct != null)
            {
                if (!dummyProduct.ParentProduct.AddPart(dummyProduct))
                {
                    await DeleteProduct(dummyProduct.Id);
                    await _context.SaveChangesAsync();
                    return BadRequest("This product doesn't fit in the parent product's parts\nProduct not created");
                }
            }
            
            // parts
            foreach (var part in product.Parts)
            {
                var partFromId = GetProductFromId(part.Id);
                
                // if the part doesn't exist, it isn't added to the product
                if (partFromId != null)
                {
                    var added = dummyProduct.AddPart(partFromId);
                    if (added)
                    {
                        partFromId.ParentProduct = dummyProduct;
                        partFromId.ParentProductId = dummyProduct.Id;
                    }
                }
            }

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduct", new { id = dummyProduct.Id }, dummyProduct.toDTO());
        }

        // DELETE: api/Product/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = GetProductFromId(id);
            
            if (product == null)
            {
                return NotFound();
            }
            
            _context.Entry(product).State = EntityState.Deleted;
            foreach (var part in product.Parts)
            {
                await DeleteProduct(part.Id);
            }
            await _context.SaveChangesAsync();

            return Ok(product.toDTO());
        }

        private bool ProductExists(long id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}