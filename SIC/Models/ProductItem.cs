//using System;
//using System.Collections.Generic;
//using System.Linq;
//using SIC.DTOs;
//
//namespace SIC.Models
//{
//    public class ProductItem
//    {
//        public long Id { get; set; }
//        
//        /**
//         * This ProductItem has the following attributes from the AbstractProduct:
//         * - Name
//         * - Category
//         * - Material
//         * - Dimensions that fit
//         * - Parts that fit in the dimensions and exist
//         */
//        public Product AbstractProduct { get; set; }
//        
//        public ConcreteDimensions ConcreteDimensions { get; set; }
//        
//        public IList<ProductItem> Parts { get; set; }
//        
//        /**
//         * Converts an instance of ProductItem to its DTO equivalent
//         */
//        public ProductItemDTO toDTO() {
//            var productItem = new ProductItemDTO(){
//                Name = AbstractProduct.Name.NameContent,
//                CategoryDTO = AbstractProduct.Category.toDTO(),
//                MaterialsDTO = AbstractProduct.Materials.toDTO(),
//                ConcreteDimensions = ConcreteDimensions,
//                PartsDTO = Parts.Select(p => p.toDTO()).ToList()
//            };
//
//            return productItem;
//        }
//        
//        /**
//         * Adds a product to the list of parts of the main product
//         * Returns true if the product was successfully added, false if
//         * it does not fit in it
//         */
//        public Boolean addPart(ProductItem product)
//        {
//            double occupiedHeight = 0, occupiedWidth = 0, occupiedDepth = 0;
//            foreach (ProductItem part in Parts)
//            {
//                occupiedHeight += part.ConcreteDimensions.Height;
//                occupiedWidth += part.ConcreteDimensions.Width;
//                occupiedDepth += part.ConcreteDimensions.Depth;
//            }
//            
//            double remainingHeight = this.ConcreteDimensions.Height - (occupiedHeight);
//            double remainingWidth = this.ConcreteDimensions.Width - (occupiedWidth);
//            double remainingDepth = this.ConcreteDimensions.Depth - (occupiedDepth);
//
//            if (remainingHeight < 0 || remainingWidth < 0 || remainingDepth < 0)
//            {
//                return false;
//            }
//            this.Parts.Add(product);
//            return true;
//        }
//    }
//}