namespace SIC.Models.Restrictions
{
    public class RestrictionObligation : IRestriction<Product>
    {
        
        public bool ValidateAggregation(Product parentProduct, Product partToBeAdded)
        {
            return partToBeAdded.ParentProductId == parentProduct.Id && (bool) partToBeAdded.Mandatory;
        }
    }
}