using System.Collections.Generic;
using SIC.DTOs;

namespace SIC.Models
{
    /* ValueObject */
    public class Dimensions : ValueObject
    {
        public long Id { get; set; }
        public Dimension Height { get; set;}
        public Dimension Width { get; set;}
        public Dimension Depth { get; set;}

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Id;
            yield return Height;
            yield return Width;
            yield return Depth;
        }

        /**
        Converts an instance of Dimensions to its DTO equivalent
         */
        public DimensionsDTO toDTO() {
            var dimensions = new DimensionsDTO(){
                HeightDTO = Height.toDTO(),
                WidthDTO = Width.toDTO(),
                DepthDTO = Depth.toDTO()
            };

            return dimensions;
        }
    }
}