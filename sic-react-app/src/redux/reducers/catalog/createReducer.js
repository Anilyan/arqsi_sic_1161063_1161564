import { CREATE_PRODUCT,
    CREATE_CATEGORY,
    CREATE_MATERIAL,
    CREATE_FINISHING,
    CREATE_PRODUCT_ITEM} from '../../actions/types'

const initialState = {
    product: [],
    category: [],
    material: [],
    finishing: [],
    productItem: []
};

export default function(state = initialState, action) {
    switch (action.type) {
        case CREATE_PRODUCT:
            return {
                ...state,
                products: action.payload
            };
        case CREATE_CATEGORY:
            return {
                ...state,
                categories: action.payload
            };
        case CREATE_MATERIAL:
            return {
                ...state,
                materials: action.payload
            };
        case CREATE_FINISHING:
            return {
                ...state,
                finishing: action.payload
            };
        case CREATE_PRODUCT_ITEM:
            return {
                ...state,
                productItem: action.payload
            }
        default:
            return state;
    }
}

