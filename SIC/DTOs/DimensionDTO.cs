using System;
using System.Collections.Generic;

namespace SIC.DTOs
{
    public class DimensionDTO
    {
        public bool DiscreteDTO { get; set; }
        public IList<ValueDTO> DiscreteValuesDTO { get; set; }
        public IList<ValueDTO> ContinuousValuesDTO { get; set; }
    }
}