var mongoose            = require('mongoose');
var Schema              = mongoose.Schema;

var OrderSchema   = new Schema({
    client: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    items: [{type: mongoose.Schema.Types.ObjectId, ref: 'ProductItem'}],
    price: Number
});

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('Order', OrderSchema);