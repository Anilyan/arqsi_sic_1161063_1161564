
import React from 'react'
import { Button, Form, Dropdown, Checkbox, TextArea } from 'semantic-ui-react'
// import { countryOptions } from '../common'
const options = [
  { key: 'angular', text: 'Angular', value: 'angular' },
  { key: 'css', text: 'CSS', value: 'css' },
  { key: 'design', text: 'Graphic Design', value: 'design' },
  { key: 'ember', text: 'Ember', value: 'ember' },
  { key: 'html', text: 'HTML', value: 'html' },
  { key: 'ia', text: 'Information Architecture', value: 'ia' },
  { key: 'javascript', text: 'Javascript', value: 'javascript' },
  { key: 'mech', text: 'Mechanical Engineering', value: 'mech' },
  { key: 'meteor', text: 'Meteor', value: 'meteor' },
  { key: 'node', text: 'NodeJS', value: 'node' },
  { key: 'plumbing', text: 'Plumbing', value: 'plumbing' },
  { key: 'python', text: 'Python', value: 'python' },
  { key: 'rails', text: 'Rails', value: 'rails' },
  { key: 'react', text: 'React', value: 'react' },
  { key: 'repair', text: 'Kitchen Repair', value: 'repair' },
  { key: 'ruby', text: 'Ruby', value: 'ruby' },
  { key: 'ui', text: 'UI Design', value: 'ui' },
  { key: 'ux', text: 'User Experience', value: 'ux' },
];


const Update = () => (
  <div class="Product-data">
    <Form>
      <div class="Product-dimensions">
        <Form.Group widths='equal'>
          <Dropdown placeholder='Select Product' fluid search selection options={options} />
          <Button type='submit'>Submit</Button>
        </Form.Group>
      </div>
    </Form>
    <Form>
    <Form.Field>
      <label>Product Name</label>
      <input placeholder='Product Name' />
    </Form.Field>
    <Form.Group widths='equal'>
      <Form.Field>
        <Dropdown placeholder='Select Category' fluid search selection options={options} />
      </Form.Field>
      <Form.Field>
        <Dropdown placeholder='Select Materials' fluid multiple selection options={options} />
      </Form.Field>
    </Form.Group>
    <Form.Group widths='equal'>
      <div class="Product-dimensions">
        <Form.Group widths='equal'>
          <Form.Field>
            <input placeholder='Width' />
          </Form.Field>
          <Button type='submit'>Add</Button>
        </Form.Group>
      </div>
      <div class="Product-dimensions">
        <Form.Group widths='equal'>
          <Form.Field>
            <input placeholder='Height' />
          </Form.Field>
          <Button type='submit'>Add</Button>
        </Form.Group>
      </div>
      <div class="Product-dimensions">
        <Form.Group widths='equal'>
          <Form.Field>
            <input placeholder='Depth' />
          </Form.Field>
          <Button type='submit'>Add</Button>
        </Form.Group>
      </div>  
    </Form.Group>
    <Form.Group widths='equal'>
      <Form.Field>
        <Dropdown placeholder='Select Parent' fluid search selection options={options} />
      </Form.Field>
      <Form.Field>
        <Dropdown placeholder='Select Parts' fluid multiple selection options={options} />
      </Form.Field>
    </Form.Group>
    <Form.Group widths='equal'>
      <Form.Field>
        <Checkbox label='Mandatory' />
      </Form.Field>
      <Form.Input
        fluid
        id='form-subcomponent-min-occupation'
        label='Min Occupation'
        placeholder='Min Occupation'
      />
      <Form.Input
        fluid
        id='form-subcomponent-max-occupation'
        label='Max Occupation'
        placeholder='Max Occupation'
      />
    </Form.Group>
    
    <Button type='submit'>Submit</Button>
  </Form>
  </div>
);

export default Update;
