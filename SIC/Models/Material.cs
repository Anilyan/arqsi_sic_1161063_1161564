using System.Collections.Generic;
using System.Linq;
using SIC.DTOs;

namespace SIC.Models
{
    
    public class Material
    {
        public long Id { get; set; }

        public Name Designation { get; set; }

        public List<Finishing> FinishingList { get; set; }

        /**
        Converts an instance of Material to its DTO equivalent
         */
        public MaterialDTO ToDTO() {
            var material = new MaterialDTO(){
                Designation =  Designation.NameContent,
                FinishingList = FinishingList == null ? new List<FinishingDTO>() 
                    : FinishingList.Count == 0 ? new List<FinishingDTO>() 
                    : FinishingList.Select(fl => fl.toDTO()).ToList()
            };

            return material;
        }
    }
}