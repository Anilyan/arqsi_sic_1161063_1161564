import React, { Component } from 'react';
import { Input, Menu, Segment } from 'semantic-ui-react'
import { Container, Image, List } from 'semantic-ui-react'
import {ProductItem} from './productItem'

export default class Customize extends Component {
    state = { activeItem: 'home' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (

    <Container name='customize'>
        <p></p>
        <ProductItem></ProductItem>
    </Container>
    )
  }
}
