import axios from 'axios';

import {
    CREATE_PRODUCT,
    CREATE_CATEGORY,
    CREATE_MATERIAL,
    CREATE_FINISHING, CREATE_PRODUCT_ITEM
} from './types'

export const createProduct = (product) => dispatch => {
    axios.post('http://localhost:5001/api/Product', product)
        .then(product => {
            dispatch({
                type: CREATE_PRODUCT,
                payload: product
            })
        });
};

export const createProductItem = (productItem) => dispatch => {
    axios.post('http://localhost:3001/productItem', productItem)
        .then(productItem => {
            dispatch({
                type: CREATE_PRODUCT_ITEM,
                payload: productItem
            })
        })
};

export const createFinishing = (finishing) => dispatch => {
    axios.post('http://localhost:5001/api/Finishing', finishing)
        .then(finishing => {
            dispatch({
                type: CREATE_FINISHING,
                payload: finishing
            })
        });
};

export const createMaterial = (material) => dispatch => {
    axios.post('http://localhost:5001/api/Material', material)
        .then(material => {
            dispatch({
                type: CREATE_MATERIAL,
                payload: material
            })
        });
};