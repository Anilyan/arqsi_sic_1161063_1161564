using System.Collections.Generic;
using SIC.Models;

namespace SIC.DTOs
{
    public class ProductItemDTO
    {
        public string Name { get; set; }

        public CategoryDTO CategoryDTO { get; set; }
        
        public MaterialDTO MaterialsDTO { get; set; }

        public ConcreteDimensions ConcreteDimensions { get; set; }

        public IList<ProductItemDTO> PartsDTO { get; set; }
    }
}