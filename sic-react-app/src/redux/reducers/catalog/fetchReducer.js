import { FETCH_PRODUCTS, FETCH_PRODUCT_BY_ID,
    FETCH_CATEGORIES,
    FETCH_MATERIALS,
    FETCH_FINISHING,
    FETCH_PRODUCT_ITEMS, FETCH_PRODUCT_ITEM_BY_ID } from '../../actions/types'

const initialState = {
    products: [],
    productById: [],
    categories: [],
    materials: [],
    finishing: [],
    productItems: [],
    productItemById: []
};

export default function(state = initialState, action) {
    switch (action.type) {
        case FETCH_PRODUCTS:
            return {
                ...state,
                products: action.payload
            };
        case FETCH_PRODUCT_BY_ID:
            return {
                ...state,
                productById: action.payload
            };
        case FETCH_CATEGORIES:
            return {
                ...state,
                categories: action.payload
            };
        case FETCH_MATERIALS:
            return {
                ...state,
                materials: action.payload
            };
        case FETCH_FINISHING:
            return {
                ...state,
                finishing: action.payload
            };
        case FETCH_PRODUCT_ITEMS:
            return {
                ...state,
                productItems: action.payload
            };
        case FETCH_PRODUCT_ITEM_BY_ID:
            return {
                ...state,
                productItemById: action.payload
            };
        default:
            return state;
    }
}

