using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SIC.DTOs;

namespace SIC.Models
{
    /* ValueObject
     * Values are in centimeters
     * Can represent Height, Width or Depth
     */
    public class Dimension : ValueObject
    {
        public long Id { get; set; }
        
        /* True if the values are discrete, false if they are continuous */
        public bool Discrete { get; set; }

        /* If Discrete is false, DiscreteValues is null */
//        [NotMapped]
        public IList<Value> DiscreteValues { get; set; }

        /* If Discrete is true, ContinuousValues is null */
//        [NotMapped]
        public IList<Value> ContinuousValues { get; set; }

        /**
        Converts an instance of Dimensions to its DTO equivalent
         */
        public DimensionDTO toDTO()
        {
            DimensionDTO dimension;
            if (Discrete)
            {
                dimension = new DimensionDTO(){
                    DiscreteDTO = Discrete,
//                    DiscreteValuesDTO = DiscreteValues
                    DiscreteValuesDTO = DiscreteValues.Select(dv => dv.toDTO()).ToList()
                };
            }
            else
            {
                dimension = new DimensionDTO(){
                    DiscreteDTO = Discrete,
//                    ContinuousValuesDTO = ContinuousValues
                    ContinuousValuesDTO = ContinuousValues.Select(cv => cv.toDTO()).ToList()
                };
            }

            return dimension;
        }
        
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Id;
            yield return Discrete;
            yield return DiscreteValues;
            yield return ContinuousValues;
        }
    }
}