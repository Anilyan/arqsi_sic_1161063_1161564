import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import {render} from 'react-dom';
import './index.css';
import {BrowserRouter} from 'react-router-dom'
import Routes from './Routes';
import {NavBar} from './modules';

import {Provider} from 'react-redux';
import store from './redux/store';
    
render(
    <Provider store={store}>
        <div>
            <NavBar/>
            <Routes/>
        </div>
    </Provider>,
    document.getElementById('root')
);
