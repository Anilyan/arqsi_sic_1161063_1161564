import React from 'react';
import { Container, Grid, Image } from 'semantic-ui-react'
import logo from '../../logo.svg';
import '../../App.css';
import {Catalog, Customize, Cart} from '../business'
import {Login, Signup} from '../auth'
import img1 from '../../resources/img1.png'
import img2 from '../../resources/img2.png'
import img3 from '../../resources/img3.png'
import img4 from '../../resources/img4.png'
import img5 from '../../resources/img5.png'
import img6 from '../../resources/img6.png'
import img7 from '../../resources/img7.png'
import img8 from '../../resources/img8.png'
import img9 from '../../resources/img9.png'

const HomeView = () => (
        <Container name="header" className="App-home">
            <div class="Home-title">
                <img src={logo} alt="logo" class="App-logo"/>
                <h1 class="App-title">Stock In Closet</h1>
            </div>
            <Grid>
                <Grid.Row columns={5}>
                <Grid.Column>
                    <img src={img3} class="img-swap" />
                </Grid.Column>
                <Grid.Column>
                    <img src={img2} class="img-swap" />
                </Grid.Column>
                <Grid.Column>
                    <img src={img1} class="img-swap" />
                </Grid.Column>
                <Grid.Column>
                    <img src={img4} class="img-swap" />
                </Grid.Column>
                <Grid.Column>
                    <img src={img5} class="img-swap" />
                </Grid.Column>
                </Grid.Row>

                <Grid.Row columns={4}>
                <Grid.Column>
                    <img src={img6} class="img-swap" />
                </Grid.Column>
                <Grid.Column>
                    <img src={img7} class="img-swap" />
                </Grid.Column>
                <Grid.Column>
                    <img src={img8} class="img-swap" />
                </Grid.Column>
                <Grid.Column>
                    <img src={img9} class="img-swap" />
                </Grid.Column>
                </Grid.Row>

            </Grid>
        </Container>
);

export default HomeView;