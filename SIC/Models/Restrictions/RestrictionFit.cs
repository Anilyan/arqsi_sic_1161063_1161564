using System.Linq;

namespace SIC.Models.Restrictions
{
    public class RestrictionFit : IRestriction<Product>
    {
        public bool ValidateAggregation(Product parentProduct, Product partToBeAdded)
        {
            // list of mandatory parts (according to the Restriction Obligation)
            var mandatoryParts = parentProduct.Parts
                .Where(productPart => new RestrictionObligation().ValidateAggregation(parentProduct, productPart))
                .ToList();
            
            // check how much space is being occupied with the minimum values
            double occupiedHeight = 0, occupiedWidth = 0, occupiedDepth = 0;
            foreach (var mandatoryPart in mandatoryParts)
            {
                occupiedHeight += mandatoryPart.Dimensions.Height.Discrete
                    ? mandatoryPart.Dimensions.Height.DiscreteValues.Min().ValueDouble
                    : mandatoryPart.Dimensions.Height.ContinuousValues.Min().ValueDouble;
                occupiedWidth += mandatoryPart.Dimensions.Width.Discrete
                    ? mandatoryPart.Dimensions.Width.DiscreteValues.Min().ValueDouble
                    : mandatoryPart.Dimensions.Width.ContinuousValues.Min().ValueDouble;
                occupiedDepth += mandatoryPart.Dimensions.Depth.Discrete
                    ? mandatoryPart.Dimensions.Depth.DiscreteValues.Min().ValueDouble
                    : mandatoryPart.Dimensions.Depth.ContinuousValues.Min().ValueDouble;
            }
            
            // check how much space is left at most with the maximum value of dimensions of the product
            var remainingHeight = parentProduct.Dimensions.Height.Discrete
                ? parentProduct.Dimensions.Height.DiscreteValues.Max().ValueDouble - occupiedHeight
                : parentProduct.Dimensions.Height.ContinuousValues.Max().ValueDouble - occupiedHeight;
            var remainingWidth = parentProduct.Dimensions.Width.Discrete
                ? parentProduct.Dimensions.Width.DiscreteValues.Max().ValueDouble - occupiedWidth
                : parentProduct.Dimensions.Width.ContinuousValues.Max().ValueDouble - occupiedWidth;
            var remainingDepth = parentProduct.Dimensions.Depth.Discrete
                ? parentProduct.Dimensions.Depth.DiscreteValues.Max().ValueDouble - occupiedDepth
                : parentProduct.Dimensions.Depth.ContinuousValues.Max().ValueDouble - occupiedDepth;
            
            // if the part has the minimum values, check how much space is left
            remainingHeight -= partToBeAdded.Dimensions.Height.Discrete
                ? partToBeAdded.Dimensions.Height.DiscreteValues.Min().ValueDouble
                : partToBeAdded.Dimensions.Height.ContinuousValues.Min().ValueDouble;
            remainingWidth -= partToBeAdded.Dimensions.Width.Discrete
                ? partToBeAdded.Dimensions.Width.DiscreteValues.Min().ValueDouble
                : partToBeAdded.Dimensions.Width.ContinuousValues.Min().ValueDouble;
            remainingDepth -= partToBeAdded.Dimensions.Depth.Discrete
                ? partToBeAdded.Dimensions.Depth.DiscreteValues.Min().ValueDouble
                : partToBeAdded.Dimensions.Depth.ContinuousValues.Min().ValueDouble;
            
            // if the space left is negative, the part doesn't fit
            if (remainingHeight < 0 || remainingWidth < 0 || remainingDepth < 0)
            {
                return false;
            }
            
            // checks if the part to be added is within the range of occupation the parent product
            // has destined to it
            return new RestrictionOccupation().ValidateAggregation(parentProduct, partToBeAdded);
        }
    }
}