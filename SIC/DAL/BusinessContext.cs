using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using SIC.Models;

namespace SIC.DAL
{
    public class BusinessContext : DbContext {
        public BusinessContext(DbContextOptions<BusinessContext> options)
            : base(options)
        { }

        public DbSet<Product> Products { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Material> Materials { get; set; }

        public DbSet<Finishing> Finishings { get; set; }

        public DbSet<Dimensions> Dimensions { get; set; }
        
        public DbSet<Dimension> Dimension { get; set; }

//        public DbSet<SIC.Models.ProductItem> ProductItem { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Product>()
                .HasMany(p => p.Parts)
                .WithOne(p => p.ParentProduct)
                .HasForeignKey(pp => pp.ParentProductId);
            
            modelBuilder.Entity<Category>()
                .HasMany(p => p.SubCategories)
                .WithOne(sc => sc.ParentCategory)
                .HasForeignKey(pc => pc.ParentCategoryId);
        }

    }
}