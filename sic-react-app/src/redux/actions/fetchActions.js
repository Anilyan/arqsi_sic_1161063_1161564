import axios from 'axios';

import {
    FETCH_PRODUCTS, FETCH_PRODUCT_BY_ID,
    FETCH_CATEGORIES,
    FETCH_MATERIALS,
    FETCH_FINISHING,
    FETCH_PRODUCT_ITEMS,
    FETCH_PRODUCT_ITEM_BY_ID
} from './types'

export const fetchProducts = () => dispatch => {
    axios.get('http://localhost:5001/api/Product')
        .then(products => {
            dispatch({
                type: FETCH_PRODUCTS,
                payload: products
            })
        })
};

export const fetchProductById = (id) => dispatch => {
    axios.get('http://localhost:5001/api/Product/' + id)
        .then(product => {
            dispatch({
                type: FETCH_PRODUCT_BY_ID,
                payload: product
            })
        })
};

export const fetchCategories = () => dispatch => {
    axios.get('http://localhost:5001/api/Category')
        .then(categories => {
            dispatch({
                type: FETCH_CATEGORIES,
                payload: categories
            })
        })
};

export const fetchMaterials = () => dispatch => {
    axios.get('http://localhost:5001/api/Material')
        .then(materials => {
            dispatch({
                type: FETCH_MATERIALS,
                payload: materials
            })
        })
};

export const fetchFinishing = () => dispatch => {
    axios.get('http://localhost:5001/api/Finishing')
        .then(finishing => {
            dispatch({
                type: FETCH_FINISHING,
                payload: finishing
            })
        })
};

export const fetchProductItems = () => dispatch => {
    axios.get('http://localhost:3001/productItem')
        .then(productItem => {
            dispatch({
                type: FETCH_PRODUCT_ITEMS,
                payload: productItem
            })
        })
};

export const fetchProductItemById = (id) => dispatch => {
    axios.get('http://localhost:3001/productItem/' + id)
        .then(productItem => {
            dispatch({
                type: FETCH_PRODUCT_ITEM_BY_ID,
                payload: productItem
            })
        })
};