using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIC.DAL;
using SIC.DTOs;
using SIC.Models;

namespace SIC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly BusinessContext _context;

        public CategoryController(BusinessContext context)
        {
            _context = context;
            if(!_context.Categories.Any()){
//                var category1 = new Category{Name = new Name{NameContent = "category1"}, SubCategories = new List<Category>()};
//                var category2 = new Category{Name = new Name{NameContent = "category2"}, SubCategories = new List<Category>()};
//                List<Category> categories1 = new List<Category>{category1, category2};
//                var category3 = new Category{Name = new Name{NameContent = "category3"}, SubCategories = categories1};
//                var category4 = new Category{Name = new Name{NameContent = "category4"}, SubCategories = new List<Category>()};
//                List<Category> categories2 = new List<Category>{category3, category4};
//                var category5 = new Category{Name = new Name{NameContent = "category5"}, SubCategories = categories2};
//                category3.ParentCategory = category5;
//                category4.ParentCategory = category5;
//                category1.ParentCategory = category3;
//                category2.ParentCategory = category3;
//                _context.Categories.Add(category1);
//                _context.Categories.Add(category2);
//                _context.Categories.Add(category3);
//                _context.Categories.Add(category4);
//                _context.Categories.Add(category5);
                _context.SaveChanges();
            }
        }

        // GET: api/Category
        [HttpGet]
        public IEnumerable<CategoryDTO> GetCategories()
        {
            var categories = from c in _context.Categories
                .Include(c => c.Name)
                .Include(c => c.ParentCategory)
                .Include(c => c.SubCategories).ThenInclude(sc => sc.Name)
             select c.toDTO();
            return categories;
        }

        // GET: api/Category/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategory([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Category category = GetCategoryFromId(id);
            
            if (category == null)
            {
                return NotFound();
            }

            return Ok(category.toDTO());
        }
        
        /**
         * Returns the category within the database with the specified id
         * Null if there is no category with this id
         */
        public Category GetCategoryFromId(long id)
        {

            IEnumerable<Category> categories = _context.Categories
                .Include(c => c.Name)
                .Include(c => c.ParentCategory)
                .Include(c => c.SubCategories).ThenInclude(sc => sc.Name);

            Category categoryToReturn = null;
            foreach (var category in categories)
            {
                if (category.Id.Equals(id))
                {
                    categoryToReturn = category;
                    break;
                }
            }

            return categoryToReturn;
        }

        // PUT: api/Category/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategory([FromRoute] long id, [FromBody] Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            if (!CategoryExists(id))
            {
                return NotFound();
            }

            var categoryToChange = GetCategoryFromId(id);
            
            // if the parent category id is the same, nothing about this is changed
            if (category.ParentCategoryId != categoryToChange.ParentCategoryId)
            {
                Category parentCategory = null;
                if (category.ParentCategoryId != null)
                {
                    // if the parent category to be added doesn't exist, the process stops
                    parentCategory = GetCategoryFromId((long) category.ParentCategoryId);
                    if (parentCategory == null)
                    {
                        return NotFound("Category with id " + category.ParentCategoryId + " was not found\nCategory not updated");
                    }
                    
                    parentCategory.SubCategories.Add(categoryToChange);
                }
                
                if (categoryToChange.ParentCategoryId != null)
                {
                    // remove this category from the sub categories of the parent category
                    categoryToChange.ParentCategory.SubCategories.Remove(category);
                    categoryToChange.ParentCategory = null;
                }
                
                categoryToChange.ParentCategoryId = category.ParentCategoryId;
                categoryToChange.ParentCategory = parentCategory;
            }
            
            if (category.Name != null)
            {
                categoryToChange.Name = category.Name;
            }

            var auxiliaryList = new List<Category>(categoryToChange.SubCategories);

            // deletes the sub categories in the category that are not to be kept
            foreach (var subCategory in auxiliaryList)
            {
                var keepSubCat = false;
                foreach (var categorySubCategory in category.SubCategories)
                {
                    if (categorySubCategory.Id == subCategory.Id)
                    {
                        keepSubCat = true;
                        break;
                    }
                }

                if (!keepSubCat)
                {
                    await DeleteCategory(subCategory.Id);
                }
            }
            
            // add the new sub categories
            foreach (var subCategory in category.SubCategories)
            {
                var subCategoryFromId = GetCategoryFromId(subCategory.Id);
                
                // if the sub category doesn't exist, it isn't added to the category
                if (subCategoryFromId != null && !categoryToChange.SubCategories.Contains(subCategoryFromId))
                {
                    categoryToChange.SubCategories.Add(subCategoryFromId);
                    subCategoryFromId.ParentCategory = categoryToChange;
                    subCategoryFromId.ParentCategoryId = id;
                }
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCategory", new { id = categoryToChange.Id }, categoryToChange.toDTO());
        }

        // POST: api/Category
        [HttpPost]
        public async Task<IActionResult> PostCategory([FromBody] Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            // parent category
            long? parentCategoryId = null;
            Category parentCategory = null;
            if (category.ParentCategoryId != null)
            {
                parentCategory = GetCategoryFromId((long) category.ParentCategoryId);
                if (parentCategory == null)
                {
                    return NotFound("Category with id " + category.ParentCategoryId + " was not found\nCategory not created");
                }
                parentCategoryId = category.ParentCategoryId;
            }
            
            var dummyCategory = new Category
            {
                Name = category.Name,
                ParentCategory = parentCategory,
                ParentCategoryId = parentCategoryId,
                SubCategories = new List<Category>()
            };
            
            // sub categories
            foreach (var subCategory in category.SubCategories)
            {
                var subCategoryFromId = GetCategoryFromId(subCategory.Id);
                
                // if the sub category doesn't exist, it isn't added to the category
                if (subCategoryFromId != null)
                {
                    dummyCategory.SubCategories.Add(subCategoryFromId);
                    subCategoryFromId.ParentCategory = dummyCategory;
                    subCategoryFromId.ParentCategoryId = dummyCategory.Id;
                }
            }

            dummyCategory.ParentCategory?.SubCategories.Add(dummyCategory);

            _context.Categories.Add(dummyCategory);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCategory", new { id = dummyCategory.Id }, dummyCategory.toDTO());
        }

        // DELETE: api/Category/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var category = GetCategoryFromId(id);
            
            if (category == null)
            {
                return NotFound();
            }

            // any product with these categories associated will then have them at null
            _context.Entry(category).State = EntityState.Deleted;
            foreach (var categorySubCategory in category.SubCategories)
            {
                await DeleteCategory(categorySubCategory.Id);
            }
            await _context.SaveChangesAsync();

            return Ok(category.toDTO());
        }

        private bool CategoryExists(long id)
        {
            return _context.Categories.Any(e => e.Id == id);
        }
    }
}