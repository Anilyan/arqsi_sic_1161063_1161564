using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SIC.DTOs;
using SIC.Models.Restrictions;

namespace SIC.Models
{
    public class Product
    {
        public long Id { get; set; }

        public Name Name { get; set; }

        public Category Category { get; set; }

        public Dimensions Dimensions { get; set; }

        public List<Material> Materials { get; set; }

        //tree structure
        public long? ParentProductId { get; set; }
        public Product ParentProduct { get; set; }
        public IList<Product> Parts { get; set; }
        
        //Restriction Obligation
        public bool? Mandatory { get; set; }
        
        // Restriction Occupation
        public long MaxOccupation { get; set; }
        public long MinOccupation { get; set; }
        
        /**
         * Converts an instance of Product to its DTO equivalent
         */
        public ProductDTO toDTO() {
            var product = new ProductDTO(){
                Id = Id,
                ProductName =  Name.NameContent,
                CategoryId = Category.Id,
                Dimensions = Dimensions.toDTO(),
                Materials = Materials.Count != 0 ? 
                    Materials.Select(m => m.ToDTO()).ToList()
                    : new List<MaterialDTO>(),
                Mandatory = Mandatory,
                MaxOccupation = MaxOccupation,
                MinOccupation = MinOccupation,
                ParentProductId = ParentProductId,
                Parts = Parts.Count != 0 ? 
                    Parts.Select(p => p.Id).ToList()
                    : new List<long>()
            };

            return product;
        }

        /**
         * Adds the part given by parameter according to the restrictions in use:
         * - Restriction Obligation
         * - Restriction Fit
         * - Restriction Occupation
         *
         * All the mandatory parts must be present, the optionals must fit within
         * the space that the mandatory don't use
         *
         * The part is only added if its dimensions are within the range of occupation
         * that the parent determines
         *
         * Returns true if the product was successfully added as a part, false if
         * it doesn't fit
         */
        public bool AddPart(Product part)
        {
            //var fits = new RestrictionFit().ValidateAggregation(this, part);
            if (/*fits*/true)
            {
                Parts.Add(part);
            }
            return /*fits*/true;
        }
    }
}

