import React, { Component } from 'react'
import { Menu, Segment, Container } from 'semantic-ui-react'
import history from '../history';
import logo from '../../logo.svg';
import styles from '../../App.css';
import {Catalog, Customize, Cart} from '../business'
import {Login, Signup} from '../auth'
import {HomeView} from '../homepage'

// export {Customize, Catalog, Cart};

export default class NavBar extends Component {
  state = { activeItem: 'home' }


  handleItemClick = (e, { name }) => this.setState({ activeItem: name, activeSegment: name });

  // loadHome = (e, { name }) => {
  //   this.setState({ activeItem: name});
  //   history.push("/");
  // };

  // loadLogin = (e, { name }) => {
  //   this.setState({ activeItem: name });
  //   history.push("/login");
  // };

  // loadSignUp = (e, { name }) => {
  //   this.setState({ activeItem: name });
  //   history.push("/signup");
  // };

  // loadCatalog = (e, { name }) => {
  //   this.setState({ activeItem: name });
  //   history.push("/catalog");
  // };

  // loadCustomize = (e, { name }) => {
  //   this.setState({ activeItem: name });
  //   history.push("/customize");
  // };

  // loadCart = (e, { name }) => {
  //   this.setState({ activeItem: name });
  //   history.push("/cart");
  // };

  render() {
    const { activeItem } = this.state

    let pages = {
      home: (
          <Container name='home'>
              <HomeView></HomeView>
          </Container>
      ),

      catalog: (
          <Container name='catalog'>
              <Catalog></Catalog>
          </Container>
      ),
          
      customize: (
          <Container name='customize'>
              <Customize></Customize>
          </Container>
      ),

      cart: (
          <Container name='cart'>
              <Cart></Cart>
          </Container>
      ),

      login: (
          <Container name='login'>
              <Login></Login>
          </Container>
      ),

      signup: (
          <Container name='signup'>
              <Signup></Signup>
          </Container>
      ),
     
     };

    return (
      <div>
        <Menu pointing secondary>
          <Menu.Menu>
            <Menu.Item 
              name='home' 
              content="Home"
              active={activeItem === 'home'} 
              // onClick={this.loadHome}
              onClick={this.handleItemClick}
            />
            <Menu.Item 
              name='catalog' 
              content="Catalog"
              active={activeItem === 'catalog'} 
              // onClick={this.loadCatalog}
              onClick={this.handleItemClick}
            />
            <Menu.Item 
              name='customize' 
              content="Customize"
              active={activeItem === 'customize'} 
              // onClick={this.loadCustomize}
              onClick={this.handleItemClick}
            />
          </Menu.Menu>
          <Menu.Menu position='right'>
            <Menu.Item 
              name='cart' 
              content="Cart"
              icon="cart"
              active={activeItem === 'cart'} 
              // onClick={this.loadCart}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name='login'
              content="Login"
              active={activeItem === 'login'}
              // onClick={this.loadLogin}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name='signup'
              content="Sign up"
              active={activeItem === 'signup'}
              // onClick={this.loadSignUp}
              onClick={this.handleItemClick}
            />
          </Menu.Menu>
        </Menu>

      {pages[this.state.activeItem]}
      </div>
    )
  }
}
