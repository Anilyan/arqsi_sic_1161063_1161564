import React, { Component } from 'react';
import { Input, Menu, Segment } from 'semantic-ui-react'
import { Container, Image, List } from 'semantic-ui-react'
import {Create, Delete, Find, Update} from './index'
import img from '../../../resources/product.png'

import {connect} from "react-redux";
import {fetchCategories} from "../../../redux/actions/fetchActions";

class Category extends Component {
  state = { activeItem: 'create' };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  componentDidMount() {
      this.props.fetchCategories();
  }

  render() {
    const { activeItem } = this.state;

    let pages = {
        create: (
            <Container name='create'>
                <Create></Create>
            </Container>
        ),

        update: (
            <Container name='find'>
                <Update></Update>
            </Container>
        ),

        delete: (
            <Container name='delete'>
                <Delete></Delete>
            </Container>
        ),
       
    };
   
    let renderActivePage = () => pages[this.state.activeItem];

      var categories;
      if (this.props.products.data != null) {

          categories = this.props.products.data.map(category => (
              <div key={category.id} className="Product-presentation">
                  <div style={{marginTop: -22 + 'px'}}>
                      <img
                          src={img}
                          className="Product-img"/>
                      <List.Content>
                          <div className="Product-info">
                              <List.Header><b>{category.name}</b></List.Header>
                              <div style={{marginTop: -5 + 'px'}}>sample description</div>
                          </div>
                      </List.Content>
                  </div>
              </div>
          ));

      }

    return (
      <div>
          <List position="left" class="ProductListContainer" celled>
              <h1><center>Categories list</center></h1>
              {categories}
          </List>

        <div class="Product-actions">
            <Menu attached='top' tabular>
            <Menu.Item name='create' 
                active={activeItem === 'create'} 
                onClick={this.handleItemClick} />
            <Menu.Item
                name='update'
                active={activeItem === 'update'}
                onClick={this.handleItemClick}
            />
            <Menu.Item
                name='delete'
                active={activeItem === 'delete'}
                onClick={this.handleItemClick}
            />
            </Menu>
        </div>
        

        {pages[this.state.activeItem]}

      </div>
    )
  }
}

const mapStateToProps = state => ({
    products: state.products.categories
});

export default connect(mapStateToProps, { fetchCategories })(Category);