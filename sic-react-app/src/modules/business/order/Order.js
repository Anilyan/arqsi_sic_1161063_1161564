import React, { Component } from 'react';
import { Input, Menu, Segment } from 'semantic-ui-react'
import { Container, Image, List } from 'semantic-ui-react'
import {Create, Delete, Find, Update} from './index'
import img from '../../../resources/order.png'

export default class Order extends Component {
  state = { activeItem: 'create' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    let pages = {
        create: (
            <Container name='create'>
                <Create></Create>
            </Container>
        ),
            
        find: (
            <Container name='find'>
                <Find></Find>
            </Container>
        ),

        update: (
            <Container name='find'>
                <Update></Update>
            </Container>
        ),

        delete: (
            <Container name='delete'>
                <Delete></Delete>
            </Container>
        ),
       
       };
   
       let renderActivePage = () => pages[this.state.activeItem];

    return (
      <div>
          <List position="left" class="ProductListContainer" celled>
            <h1><center>Ordered items</center></h1>
            <div class="Product-presentation">
                <div style={{marginTop: -22 + 'px'}} >
                <img src={img} class="Product-img"/>
                    <List.Content>
                        <div class="Product-info">
                            <List.Header><b>O1</b></List.Header>
                            <div style={{marginTop: -5 + 'px'}} >sample description</div>
                        </div>
                    </List.Content>
                </div>
            </div>
            <div class="Product-presentation">
                <div style={{marginTop: -22 + 'px'}} >
                <img src={img} class="Product-img"/>
                    <List.Content>
                        <div class="Product-info">
                            <List.Header><b>O2</b></List.Header>
                            <div style={{marginTop: -5 + 'px'}} >sample description</div>
                        </div>
                    </List.Content>
                </div>
            </div>
            <div class="Product-presentation">
                <div style={{marginTop: -22 + 'px'}} >
                <img src={img} class="Product-img"/>
                    <List.Content>
                        <div class="Product-info">
                            <List.Header><b>O3</b></List.Header>
                            <div style={{marginTop: -5 + 'px'}} >sample description</div>
                        </div>
                    </List.Content>
                </div>
            </div>
        </List>

        <div class="Product-actions">
            <Menu attached='top' tabular>
            <Menu.Item name='create' 
                active={activeItem === 'create'} 
                onClick={this.handleItemClick} />
            <Menu.Item
                name='find'
                active={activeItem === 'find'}
                onClick={this.handleItemClick}
            />
            <Menu.Item
                name='update'
                active={activeItem === 'update'}
                onClick={this.handleItemClick}
            />
            <Menu.Item
                name='delete'
                active={activeItem === 'delete'}
                onClick={this.handleItemClick}
            />
            </Menu>
        </div>
        

        {pages[this.state.activeItem]}

      </div>
    )
  }
}
