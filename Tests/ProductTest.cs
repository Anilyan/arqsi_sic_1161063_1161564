using System;
using Xunit;
using SIC.Models;
using SIC.DTOs;
using System.Collections.Generic;

namespace Tests
{
    public class ProductTest
    {
        
        private Product _product;

        public ProductTest()
        {
            
        }
        
        [Fact]
        public void testAddProduct()
        {
            Name sampleName = new Name{NameContent = "sampleName"};
            Category sampleCategory= new Category{Name = new Name{NameContent = "sampleCategory"}, 
                SubCategories = {}};
            Material sampleMaterial = new Material{Designation = new Name{NameContent = "sampleMaterial"},
                Finishing = new Finishing{Designation = new Name{NameContent = "sampleFinishing"}}};
            Dimensions sampleDimensions = new Dimensions{Height = 50, Width = 80.2, Depth = 20};
            _product = new Product{Name = sampleName, Category = sampleCategory, Materials = sampleMaterial, 
                Dimensions = sampleDimensions, Parts = new List<Product>()};
            
            Name sampleName2 = new Name{NameContent = "sampleName2"};
            Category sampleCategory2= new Category{Name = new Name{NameContent = "sampleCategory2"}, 
                SubCategories = {}};
            Material sampleMaterial2 = new Material{Designation = new Name{NameContent = "sampleMaterial2"},
                Finishing = new Finishing{Designation = new Name{NameContent = "sampleFinishing2"}}};
            Dimensions sampleDimensions2 = new Dimensions{Height = 12, Width = 42.2, Depth = 15};
            var product = new Product{Name = sampleName2, Category = sampleCategory2, Materials = sampleMaterial2, 
                Dimensions = sampleDimensions2, Parts = new List<Product>()};

            _product.addPart(product);

            var list = _product.Parts;

            foreach (Product part in list)
            {
                Assert.True(part.Name.NameContent.Equals("sampleName2"), "");
                Assert.True(part.Category.Name.NameContent.Equals("sampleCategory2"), "");
                Assert.True(part.Materials.Designation.NameContent.Equals("sampleMaterial2"), "");
                Assert.True(part.Materials.Finishing.Designation.NameContent.Equals("sampleFinishing2"), "");
                Assert.True(part.Dimensions.Height.Equals(12), "");
                Assert.True(part.Dimensions.Width.Equals(42.2), "");
                Assert.True(part.Dimensions.Depth.Equals(15), "");
            }
        }
        
        [Fact]
        public void testToDto()
        {
            Name sampleName = new Name{NameContent = "sampleName"};
            Category sampleCategory= new Category{Name = new Name{NameContent = "sampleCategory"}, 
                SubCategories = {}};
            Material sampleMaterial = new Material{Designation = new Name{NameContent = "sampleMaterial"},
                Finishing = new Finishing{Designation = new Name{NameContent = "sampleFinishing"}}};
            Dimensions sampleDimensions = new Dimensions{Height = 50, Width = 80.2, Depth = 20};
            _product = new Product{Name = sampleName, Category = sampleCategory, Materials = sampleMaterial, 
                Dimensions = sampleDimensions, Parts = new List<Product>()};

            var _productDTO = _product.toDTO();
            
            Assert.True(_productDTO.ProductName.Equals("sampleName"), "");
            Assert.True(_productDTO.CategoryDTO.Name.Equals("sampleCategory"), "");
            Assert.True(_productDTO.MaterialsDTO.Designation.Equals("sampleMaterial"), "");
            Assert.True(_productDTO.MaterialsDTO.Finishing.Designation.Equals("sampleFinishing"), "");
            Assert.True(_productDTO.DimensionsDTO.HeightDTO.Equals(50), "");
            Assert.True(_productDTO.DimensionsDTO.WidthDTO.Equals(80.2), "");
            Assert.True(_productDTO.DimensionsDTO.DepthDTO.Equals(20), "");
        }
    }
}
