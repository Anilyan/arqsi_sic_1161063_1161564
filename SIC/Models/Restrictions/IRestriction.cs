namespace SIC.Models.Restrictions
{
    public interface IRestriction<T>
    {
        bool ValidateAggregation(T parentProduct, T partToBeAdded);
    }
}