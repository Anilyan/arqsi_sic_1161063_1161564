import React, { Component } from 'react';
import { Input, Menu, Segment } from 'semantic-ui-react'
import { Container, Image, List } from 'semantic-ui-react'
import {Create, Delete, Find, Update} from './index'
import img from '../../../resources/product.png'

import {connect} from "react-redux";
import {fetchMaterials, fetchFinishing} from "../../../redux/actions/fetchActions";
import {createMaterial} from "../../../redux/actions/createActions";
import Form from "semantic-ui-react/dist/es/collections/Form/Form";
import Button from "semantic-ui-react/dist/es/elements/Button/Button";
import Dropdown from "semantic-ui-react/dist/es/modules/Dropdown/Dropdown";

class Material extends Component {
  state = { activeItem: 'create' };
  finishingList;

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

    componentDidMount() {
        this.props.fetchMaterials();
        this.props.fetchFinishing();
    }

    chooseFinishing = (e, { name, value }) => {
        this.finishingList = [];
        for (let i = 0; i < value.length; i++){
            this.finishingList.push({
                designation: {
                    nameContent: value[i]
                }
            });
        }
    };

  render() {
    const { activeItem } = this.state;

      var onCreate = () => {
          var materialToCreate = {
              designation: {
                  nameContent: document.getElementById('materialName').value
              },
              finishingList: this.finishingList
          };
          this.props.createMaterial(materialToCreate);
      };

      var options = [];
      if (this.props.finishing.data != null) {
          for (let i = 0; i < this.props.finishing.data.length; ++i){
              let finishing = this.props.finishing.data[i];
              options.push({
                  key: finishing.id,
                  text: finishing.designation,
                  value: finishing.designation
              });
          }
      }

      const Create = () => (
          <div class="Product-data">
              <Form>
                  <Form.Field>
                      <label>Material Name</label>
                      <input id='materialName' placeholder='Material Name' />
                  </Form.Field>
                  <Form.Field>
                      <Dropdown id='finishingList' placeholder='Select Finishing' onChange={this.chooseFinishing}
                                fluid multiple selection options={options} />
                  </Form.Field>
                  <Button onClick={onCreate} type='submit'>Submit</Button>
              </Form>
          </div>
      );

    let pages = {
        create: (
            <Container name='create'>
                <Create></Create>
            </Container>
        ),

        update: (
            <Container name='find'>
                <Update></Update>
            </Container>
        ),

        delete: (
            <Container name='delete'>
                <Delete></Delete>
            </Container>
        ),
       
       };
   
       let renderActivePage = () => pages[this.state.activeItem];

      var materials;
      if (this.props.products.data != null) {
          // var finishingList = this.props.products.data.finishingList;
          // var stringList;
          // console.log(this.props.products.data);
          // for (let i = 0; i < finishingList.length; i++){
          //     stringList += finishingList[i].nameContent + ", "
          // }
          materials = this.props.products.data.map(material => (
              <div key={material.id} class="Product-presentation">
                  <div style={{marginTop: -22 + 'px'}} >
                      <img src={img} class="Product-img"/>
                      <List.Content>
                          <div class="Product-info">
                              <List.Header><b>{material.designation}</b></List.Header>
                              <div style={{marginTop: -5 + 'px'}} >{material.finishingList.map(finishing => (
                                  finishing.designation + ', '
                              ))}</div>
                          </div>
                      </List.Content>
                  </div>
              </div>
          ));

      }

    return (
      <div>
          <List position="left" class="ProductListContainer" celled>
              <h1><center>Materials list</center></h1>
              {materials}
        </List>

        <div class="Product-actions">
            <Menu attached='top' tabular>
            <Menu.Item name='create' 
                active={activeItem === 'create'} 
                onClick={this.handleItemClick} />
            <Menu.Item
                name='update'
                active={activeItem === 'update'}
                onClick={this.handleItemClick}
            />
            <Menu.Item
                name='delete'
                active={activeItem === 'delete'}
                onClick={this.handleItemClick}
            />
            </Menu>
        </div>
        

        {pages[this.state.activeItem]}

      </div>
    )
  }
}

const mapStateToProps = state => ({
    products: state.products.materials,
    finishing: state.products.finishing,
    materialCreated: state.catalogCreate.material
});

export default connect(mapStateToProps, { fetchMaterials, fetchFinishing, createMaterial })(Material);