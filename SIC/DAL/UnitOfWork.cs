using System;
using SIC.Models;

/**
Pattern from 
https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application#implement-a-generic-repository-and-a-unit-of-work-class
 */
namespace SIC.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly BusinessContext _context;
        private GenericRepository<Product> productRepository;
        private GenericRepository<Category> categoryRepository;
        private GenericRepository<Dimensions> dimensionsRepository;
        private GenericRepository<Finishing> finishingRepository;
        private GenericRepository<Material> materialRepository;

        public UnitOfWork(BusinessContext context)
        {
            _context = context;
        }


        public GenericRepository<Product> ProductRepository
        {
            get
            {

                if (this.productRepository == null)
                {
                    this.productRepository = new GenericRepository<Product>(_context);
                }
                return productRepository;
            }
        }

        public GenericRepository<Category> CategoryRepository
        {
            get
            {

                if (this.categoryRepository == null)
                {
                    this.categoryRepository = new GenericRepository<Category>(_context);
                }
                return categoryRepository;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}