namespace SIC.DTOs
{
    public class DimensionsDTO
    {
        public DimensionDTO HeightDTO { get; set; }
        public DimensionDTO WidthDTO { get; set; }
        public DimensionDTO DepthDTO { get; set; }
    }
}