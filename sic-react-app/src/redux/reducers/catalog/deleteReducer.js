import { DELETE_PRODUCT, DELETE_PRODUCT_ITEM } from '../../actions/types'

const initialState = {
    product: {},
    productItem: {}
};

export default function(state = initialState, action) {
    switch (action.type) {
        case DELETE_PRODUCT:
            return {
                ...state,
                product: action.payload
            };
        case DELETE_PRODUCT_ITEM:
            return {
                ...state,
                productItem: action.payload
            };
        default:
            return state;
    }
}

