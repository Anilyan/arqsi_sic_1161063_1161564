var express = require('express');
var router = express.Router();
var Order = require('../app/models/order');
var DTO = require('../app/DTOs/orderDTO');

//General routing
router.route('/')

  .get(function (req, res) {
    Order.find(function (err, orders) {
      if (err) 
        res.send(err);
      // orders.forEach(order => {
      //   res.json(DTO.orderDTO(order))
      // });
      res.json(orders);
    });
  })

  .post(function (req, res) {
    var order = new Order();

    order.client = req.body.client;
    order.items = req.body.items;
    order.price = req.body.price;

    order.save(function (err) {
      if (err)
          res.send(err);

      res.json(order);
      // res.json(DTO.orderDTO(order));
    });

  });

//ID routing
router.route('/:id')

  .get(function (req, res) {
    Order.findById(req.params.id, function (err, order) {
      if (err) 
        res.send(err);
      
        // res.json(order);
        res.json(DTO.orderDTO(order));
    });
  })

  .put(function (req, res) {
    Order.findById(req.params.id, function (err, order) {
      if (err) 
        res.send(err);
      if(req.body.client!=null){
        order.client = req.body.client;               
      }
      if(req.body.items!=null){
        order.items = req.body.items;            
      }
      if(req.body.price!=null){
        order.price = req.body.price;            
      }
      
      order.save(function (err) {
        if (err)
            res.send(err);
  
        // res.json(order);
        res.json(DTO.orderDTO(order));
      });

    });
  })

  .delete(function (req, res) {
    Order.remove({_id: req.params.id}, function (err, order) {
      if (err) 
        res.send(err);

      res.json({message: 'Successfully deleted'});
    });

  });

module.exports = router;