module.exports = {
    GetMaterial : function (materialId){
        var Material = null;
        var Request = require("request");
        var url = "https://localhost:5001/api/Material/" + materialId;
        Request.get(url, function(err, response, body) {
            if(err) {
                return console.dir(err);
            }
            Material = JSON.parse(body);
            console.log(Material);
        });
        return Material;
    }
}