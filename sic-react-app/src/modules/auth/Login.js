import React from 'react';
import { Button, Form, Grid } from 'semantic-ui-react'

const Login = () => (
    <Grid centered>
        <Grid.Column width={6}>
        <h1>Login</h1>
        <Form>
            <Form.Field>
                <label>Email</label>
                <input type="email" placeholder='Email' />
            </Form.Field>
            <Form.Field>
                <label>Password</label>
                <input type="password" placeholder='Password' />
            </Form.Field>
            <Button type='submit'>Submit</Button>
        </Form>
        </Grid.Column>
    </Grid>
);

export default Login;