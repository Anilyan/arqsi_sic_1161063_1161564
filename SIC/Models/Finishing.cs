using SIC.DTOs;

namespace SIC.Models
{
   
    public class Finishing
    {
        public long Id { get; set; }

        public Name Designation { get; set; }

        /**
        Converts an instance of Finishing to its DTO equivalent
         */
        public FinishingDTO toDTO() {
            var finishing = new FinishingDTO(){
                Designation =  Designation.NameContent,
            };

            return finishing;
        }
    }
}