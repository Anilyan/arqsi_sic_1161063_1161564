import React from 'react';
import { Button, Checkbox, Form, Grid } from 'semantic-ui-react'

const Signup = () => (
    <Grid centered>
        <Grid.Column width={6}>
        <h1>Sign up</h1>
        <Form>
            <Form.Field>
                <label>Username</label>
                <input placeholder='Username' />
            </Form.Field>
            <Form.Field>
                <label>Email</label>
                <input type="email" placeholder='Email' />
            </Form.Field>
            <Form.Field>
                <label>Password</label>
                <input type="password" placeholder='Password' />
            </Form.Field>
            <Form.Field>
                <label>Confirm Password</label>
                <input type="password" placeholder='Confirm Password' />
            </Form.Field>
            <Form.Field>
                <Checkbox label='I agree with the terms of service' />
            </Form.Field>
            <Button type='submit'>Submit</Button>
        </Form>
        </Grid.Column>
    </Grid>
);

export default Signup;