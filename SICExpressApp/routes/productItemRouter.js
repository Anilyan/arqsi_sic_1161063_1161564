var express = require('express');
var router = express.Router();
var ProductItem = require('../app/models/productItem');
var DTO = require('../app/DTOs/productItemDTO');
var Client = require('node-rest-client').Client;
var sicClient = new Client();

//General routing
router.route('/')

  .get(function (req, res) {
    ProductItem.find(function (err, productItems) {
      if (err) 
        res.send(err);
      
      // productItems.forEach(productItem => {
      //   res.json(DTO.productItemDTO(productItem))
      // });
      res.json(productItems);
    });
  })

  .post(function (req, res) {
    var productItem = new ProductItem();

    productItem.name = req.body.name;
    productItem.product = req.body.product;
    productItem.concreteDimensions = req.body.concreteDimensions;
    productItem.parent = req.body.parent;
    productItem.parts = req.body.parts;
    productItem.cost = req.body.cost;

    var url = "https://localhost:5001/api/Product/" + productItem.product;
    sicClient.get(url, function (productReference, response) {
      var ProductReference = productReference;
      // console.log("https://localhost:5001/api/Product/" + productItem.product);
      console.log(ProductReference);
    });

    productItem.save(function (err) {
      if (err)
          res.send(err);

      res.json(productItem);
      // res.json(DTO.productItemDTO(productItem));
    });

  });

//ID routing
router.route('/:id')

  .get(function (req, res) {
    ProductItem.findById(req.params.id, function (err, productItem) {
      if (err) 
        res.send(err);
        
        // res.json(productItem);
        res.json(DTO.productItemDTO(productItem));
    });
  })

  .put(function (req, res) {
    ProductItem.findById(req.params.id, function (err, productItem) {
      if (err) 
        res.send(err);
      if(req.body.name!=null){
        productItem.name = req.body.name;           
      }
      if(req.body.product!=null){
        productItem.product = req.body.product;           
      }
      if(req.body.category!=null){
        productItem.category = req.body.category;
      }
      if(req.body.materials!=null){
        productItem.materials = req.body.materials;          
      }
      if(req.body.concreteDimensions!=null){
        productItem.concreteDimensions = req.body.concreteDimensions;         
      }
      if(req.body.parent!=null){
        productItem.parent = req.body.parent;          
      }
      if(req.body.parts!=null){
        productItem.parts = req.body.parts;                      
      }
      if(req.body.cost!=null){
        productItem.cost = req.body.cost;                      
      }
      
      productItem.save(function (err) {
        if (err)
            res.send(err);
  
        // res.json(productItem);
        res.json(DTO.productItemDTO(productItem));
      });

    });
  })

  .delete(function (req, res) {
    ProductItem.remove({_id: req.params.id}, function (err, productItem) {
      if (err) 
        res.send(err);

      res.json({message: 'Successfully deleted'});
    });

  });

module.exports = router;