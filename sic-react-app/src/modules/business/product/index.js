import Create from './Create';
import Delete from './Delete';
import Update from './Update';
import Product from './Product';

export {Create, Delete, Update, Product};