import React, {Component} from 'react';
import {Container, List, Menu} from 'semantic-ui-react'
import {Find, Update} from './index'
import img from '../../../resources/item.png'

import {fetchProductById, fetchProductItems, fetchProducts, fetchProductItemById} from "../../../redux/actions/fetchActions";
import {createProductItem} from "../../../redux/actions/createActions";
import {deleteProductItem} from "../../../redux/actions/deleteActions";
import {connect} from "react-redux";
import Form from "semantic-ui-react/dist/es/collections/Form/Form";
import Dropdown from "semantic-ui-react/dist/es/modules/Dropdown/Dropdown";
import Button from "semantic-ui-react/dist/es/elements/Button/Button";
import TextArea from "semantic-ui-react/dist/es/addons/TextArea/TextArea";

class ProductItem extends Component {
    state = {activeItem: 'create'};
    productId;
    productChosen;
    productWidth = [];
    productDepth = [];
    productHeight = [];
    productParts = [];
    findProduct;
    textArea = '';

    handleItemClick = (e, {name}) => this.setState({activeItem: name});

    componentDidMount() {
        this.props.fetchProductItems();
        this.props.fetchProducts();
    }

    componentWillReceiveProps(nextProps) {
        this.productWidth = [];
        this.productDepth = [];
        this.productHeight = [];
        this.productParts = [];
        // console.log(this.productPart);
        // console.log(this.productChosen);
        if (nextProps.productById.data != null && this.productChosen != null) {
            if (this.productChosen.dimensions.heightDTO.discreteDTO) {
                for (let i = 0; i < this.productChosen.dimensions.heightDTO.discreteValuesDTO.length; ++i) {
                    let height = this.productChosen.dimensions.heightDTO.discreteValuesDTO[i].valueDoubleDTO;
                    this.productHeight.push({
                        text: height,
                        value: height
                    });
                }
            } else {
                let minHeight = this.productChosen.dimensions.heightDTO.continuousValuesDTO[0].valueDoubleDTO;
                let maxHeight = this.productChosen.dimensions.heightDTO.continuousValuesDTO[1].valueDoubleDTO;
                for (let i = minHeight; i <= maxHeight; i++){
                    this.productHeight.push({
                        text: i,
                        value: i
                    });
                }
            }
            if (this.productChosen.dimensions.depthDTO.discreteDTO) {
                for (let i = 0; i < this.productChosen.dimensions.depthDTO.discreteValuesDTO.length; ++i) {
                    let depth = this.productChosen.dimensions.depthDTO.discreteValuesDTO[i].valueDoubleDTO;
                    this.productDepth.push({
                        text: depth,
                        value: depth
                    });
                }
            } else {
                let minDepth = this.productChosen.dimensions.depthDTO.continuousValuesDTO[0].valueDoubleDTO;
                let maxDepth = this.productChosen.dimensions.depthDTO.continuousValuesDTO[1].valueDoubleDTO;
                for (let i = minDepth; i <= maxDepth; i++){
                    this.productDepth.push({
                        text: i,
                        value: i
                    });
                }
            }
            if (this.productChosen.dimensions.widthDTO.discreteDTO) {
                for (let i = 0; i < this.productChosen.dimensions.widthDTO.discreteValuesDTO.length; ++i) {
                    let width = this.productChosen.dimensions.widthDTO.discreteValuesDTO[i].valueDoubleDTO;
                    this.productWidth.push({
                        text: width,
                        value: width
                    });
                }
            } else {
                let minWidth = this.productChosen.dimensions.widthDTO.continuousValuesDTO[0].valueDoubleDTO;
                let maxWidth = this.productChosen.dimensions.widthDTO.continuousValuesDTO[1].valueDoubleDTO;
                for (let i = minWidth; i <= maxWidth; i++){
                    this.productWidth.push({
                        text: i,
                        value: i
                    });
                }
            }
        } else {
            if (this.findProduct != null) {
                let data = nextProps.productItemById.data;
                console.log(data);

                let name = "Product Item Name: " + data.name + "; ";
                let contentString = name + "Dimensions: ";
                let width = "Width: " + data.width + " | "; //corrigir dados
                let height = "Height: " + data.height + " | ";
                let depth = "Depth: " + data.depth;
                contentString += width + height + depth + ";"
                this.textArea = contentString;
            }
        }
    }

    chooseProductForFinding = (e, {name, value}) => {
        this.findProduct = value;
    };

    chooseProductForDeletion = (e, {name, value}) => {
        this.productId = value;
    };

    chooseProductForCreation = (e, {name, value}) => {
        this.props.fetchProductById(value);
        this.productChosen = this.props.productById.data;
    };

    render() {
        const {activeItem} = this.state;

        var onFind = () => {
            this.props.fetchProductItemById(this.findProduct);
        };

        var onDelete = () => {
            console.log(this.productId);
            this.props.deleteProductItem(this.productId);
        };

        var products = [];
        // console.log(this.props.catalogProducts);
        if (this.props.catalogProducts.data != null) {
            for (let i = 0; i < this.props.catalogProducts.data.length; ++i) {
                let product = this.props.catalogProducts.data[i];
                products.push({
                    key: product.id,
                    text: product.productName,
                    value: product.id
                });
            }
        }

        var items = [];
        // console.log(this.props.products);
        if (this.props.products.data != null) {
            for (let i = 0; i < this.props.products.data.length; ++i) {
                let productItem = this.props.products.data[i];
                items.push({
                    key: productItem.id,
                    text: productItem.name,
                    value: productItem._id
                });
            }
        }

        var onCreate = () => {
            var productToCreate = {
                name: document.getElementById('productItemName').value,
                product: this.productChosen.id,
                concreteDimensions: {
                    height: document.getElementById('height').value,
                    width: document.getElementById('width').value,
                    depth: document.getElementById('depth').value
                },
                parent: null,
                parts: [],
                cost: null
            };
            this.props.createProductItem(productToCreate);
        };

        var options = [];
        const Create = () => (
            <div class="Product-data">
                <Form>
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <label>Product Item Name</label>
                            <input id='productItemName' placeholder='Product Name'/>
                        </Form.Field>
                        <Form.Field>
                            <label>Product selected</label>
                            <Dropdown placeholder='Select Product' onChange={this.chooseProductForCreation}
                                      fluid search selection options={products}/>
                        </Form.Field>
                    </Form.Group>
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <Dropdown id='width' placeholder='Specify width' fluid search selection options={this.productWidth}/>
                        </Form.Field>
                        <Form.Field>
                            <Dropdown id='height' placeholder='Specify height' fluid search selection options={this.productHeight}/>
                        </Form.Field>
                        <Form.Field>
                            <Dropdown id='depth' placeholder='Specify depth' fluid search selection options={this.productDepth}/>
                        </Form.Field>
                    </Form.Group>
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <Dropdown placeholder='Select Parent' fluid search selection options={options}/>
                        </Form.Field>
                        <Form.Field>
                            <Dropdown placeholder='Select Parts' fluid multiple selection options={this.productParts}/>
                        </Form.Field>
                    </Form.Group>
                    <Button onClick={onCreate} type='submit'>Submit</Button>
                </Form>
            </div>);

        const Delete = () => (
            <div class="Product-data">
                <Form>
                    <Dropdown placeholder='Select Product Item' onChange={this.chooseProductForDeletion}
                              fluid search selection options={items}/>
                    <p></p>
                    <Button onClick={onDelete} type='submit'>Delete</Button>
                </Form>
            </div>);

        const Find = () => (
            <div class="Product-data">
                <Form>
                    <div class="Product-dimensions">
                        <Form.Group widths='equal'>
                            <Dropdown onChange={this.chooseProductForFinding}
                                      placeholder='Select Product Item' fluid search selection options={items} />
                            <Button onClick={onFind} type='submit'>Submit</Button>
                        </Form.Group>
                    </div>
                    <TextArea id='textArea' autoHeight readOnly placeholder='Product Item data' >{this.textArea}</TextArea>
                </Form>
            </div>
        );

        let pages = {
            create: (
                <Container name='create'>
                    <Create></Create>
                </Container>
            ),

            find: (
                <Container name='find'>
                    <Find></Find>
                </Container>
            ),

            update: (
                <Container name='find'>
                    <Update></Update>
                </Container>
            ),

            delete: (
                <Container name='delete'>
                    <Delete></Delete>
                </Container>
            ),

        };

        let renderActivePage = () => pages[this.state.activeItem];

        var productItems;
        if (this.props.products.data != null) {

            productItems = this.props.products.data.map(product => (
                <div key={product.id} className="Product-presentation">
                    <div style={{marginTop: -22 + 'px'}}>
                        <img
                            src={img}
                            className="Product-img"/>
                        <List.Content>
                            <div className="Product-info">
                                <List.Header><b>{product.name}</b></List.Header>
                                <div style={{marginTop: -5 + 'px'}}></div>
                            </div>
                        </List.Content>
                    </div>
                </div>
            ));
        }

        return (
            <div>
                <List position="left" class="ProductListContainer" celled>
                    <h1>
                        <center>Created items</center>
                    </h1>
                    {productItems}
                </List>

                <div class="Product-actions">
                    <Menu attached='top' tabular>
                        <Menu.Item name='create'
                                   active={activeItem === 'create'}
                                   onClick={this.handleItemClick}/>
                        <Menu.Item
                            name='find'
                            active={activeItem === 'find'}
                            onClick={this.handleItemClick}
                        />
                        <Menu.Item
                            name='update'
                            active={activeItem === 'update'}
                            onClick={this.handleItemClick}
                        />
                        <Menu.Item
                            name='delete'
                            active={activeItem === 'delete'}
                            onClick={this.handleItemClick}
                        />
                    </Menu>
                </div>


                {pages[this.state.activeItem]}

            </div>
        )
    }
}

const mapStateToProps = state => ({
    products: state.products.productItems,
    catalogProducts: state.products.products,
    productCreated: state.products.productItems,
    productItemDeleted: state.products.productItem,
    productById: state.products.productById,
    productItemById: state.products.productItemById
});

export default connect(mapStateToProps, {
    fetchProductById,
    fetchProductItems,
    fetchProducts,
    createProductItem,
    deleteProductItem,
    fetchProductItemById
})(ProductItem);