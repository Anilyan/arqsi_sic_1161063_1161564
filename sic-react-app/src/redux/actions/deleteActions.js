import axios from 'axios';

import {
    DELETE_PRODUCT,
    DELETE_PRODUCT_ITEM
} from './types'

export const deleteProduct = (id) => dispatch => {
    axios.delete('http://localhost:5001/api/Product/' + id)
        .then(product => {
            dispatch({
                type: DELETE_PRODUCT,
                payload: product
            })
        })
};

export const deleteProductItem = (id) => dispatch => {
    axios.delete('http://localhost:3001/productItem/' + id)
        .then(product => {
            dispatch({
                type: DELETE_PRODUCT_ITEM,
                payload: product
            })
        })
};
