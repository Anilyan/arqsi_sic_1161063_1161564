import React, { Component } from 'react';
import { Input, Menu, Segment } from 'semantic-ui-react'
import { Container, Image, List } from 'semantic-ui-react'
import {Delete, Update} from './index'
import img from '../../../resources/product.png'

import Form from "semantic-ui-react/dist/es/collections/Form/Form";
import Dropdown from "semantic-ui-react/dist/es/modules/Dropdown/Dropdown";
import Button from "semantic-ui-react/dist/es/elements/Button/Button";
import TextArea from "semantic-ui-react/dist/es/addons/TextArea/TextArea";
import Checkbox from "semantic-ui-react/dist/es/modules/Checkbox/Checkbox";

import { connect } from 'react-redux';
import { fetchProducts, fetchProductById, fetchMaterials } from "../../../redux/actions/fetchActions";
import { createProduct } from "../../../redux/actions/createActions";
import { deleteProduct } from "../../../redux/actions/deleteActions";

class Product extends Component {
  state = { activeItem: 'create' };
  productId;
  textArea = '';
  materialList;

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  componentDidMount() {
      this.props.fetchProducts();
      this.props.fetchMaterials();
  }

  componentWillReceiveProps(nextProps){
      let data = nextProps.productById.data;
      if (data != null && this.productId != null){
          console.log(data);

          let productName = "Product Name: " + data.productName + "; ";
          let contentString = productName + "Materials List: ";
          for (let i = 0; i < data.materials.length; ++i){
            let material = data.materials[i];
            contentString += "[Material: " + material.designation + ", Material finishings list: ";
            for (let i = 0; i < material.finishingList.length; ++i){
                let finishing = material.finishingList[i];
                contentString += finishing.designation + ", ";
            }
            contentString += "] | ";
          }
          contentString += "; ";
          contentString += "Max Occupation: " + data.maxOccupation + "; ";
          contentString += "Min Occupation: " + data.minOccupation + "; ";
          contentString += "Dimensions: ";
          contentString += "Depth values: ";
          let depth = data.dimensions.depthDTO;
          if (depth.discreteDTO == true) {
            let values = depth.discreteValuesDTO;
            for (let i = 0; i < values.length; ++i){
                contentString += values[i].valueDoubleDTO + ", ";
            }
          } else {
            let values = depth.continuousValuesDTO;
            contentString += "[";
            for (let i = 0; i < values.length; ++i){
                contentString += values[i].valueDoubleDTO + "-";
            }
            contentString += "]";
          }
          contentString += " | ";
          contentString += "Height values: ";
          let height = data.dimensions.heightDTO;
          if (height.discreteDTO == true) {
            let values = height.discreteValuesDTO;
            for (let i = 0; i < values.length; ++i){
                contentString += values[i].valueDoubleDTO + ", ";
            }
          } else {
            let values = height.continuousValuesDTO;
            for (let i = 0; i < values.length; ++i){
                contentString += values[i].valueDoubleDTO + "-";
            }
          }
          contentString += " | ";
          contentString += "Width values: ";
          let width = data.dimensions.widthDTO;
          if (width.discreteDTO == true) {
            let values = width.discreteValuesDTO;
            for (let i = 0; i < values.length; ++i){
                contentString += values[i].valueDoubleDTO + ", ";
            }
          } else {
            let values = width.continuousValuesDTO;
            for (let i = 0; i < values.length; ++i){
                contentString += values[i].valueDoubleDTO + "-";
            }
          }
          contentString += " | ";
          contentString += "; ";
          
          this.textArea = contentString;
      }
  }

  chooseProduct = (e, { name, value }) => {
      this.productId = value;
  };

  chooseMaterials = (e, { name, value }) => {
      this.materialList = [];
      for (let i = 0; i < value.length; i++){
          this.materialList.push(value[i]);
      }
  };

  render() {
      var onFind = () => {
          this.props.fetchProductById(this.productId);
      };

      var onDelete = () => {
          console.log(this.productId);
          this.props.deleteProduct(this.productId);
      };

      var onCreate = () => {
          var dimensions, height, width, depth;

          // define width
          var splitComma = document.getElementById('widthProduct').value.split(',');
          var widths = [];
          for (let i = 0; i < splitComma.length; i++){
              widths.push({
                  valueDouble: Number(splitComma[i])
              })
          }
          if (document.getElementById('widthDiscrete').checked) {
              width = {
                  discrete: true,
                  discreteValues: widths,
                  continuousValues: null
              };
          } else {
              width = {
                  discrete: false,
                  discreteValues: null,
                  continuousValues: widths
              }
          }

          // define height
          var splitComma = document.getElementById('heightProduct').value.split(',');
          var heights = [];
          for (let i = 0; i < splitComma.length; i++){
              heights.push({
                  valueDouble: Number(splitComma[i])
              })
          }
          if (document.getElementById('heightDiscrete').checked) {
              height = {
                  discrete: true,
                  discreteValues: heights,
                  continuousValues: null
              };
          } else {
              height = {
                  discrete: false,
                  discreteValues: null,
                  continuousValues: heights
              }
          }

          // define depth
          var splitComma = document.getElementById('depthProduct').value.split(',');
          var depths = [];
          for (let i = 0; i < splitComma.length; i++){
              depths.push({
                  valueDouble: Number(splitComma[i])
              })
          }
          if (document.getElementById('depthDiscrete').checked) {
              depth = {
                  discrete: true,
                  discreteValues: depths,
                  continuousValues: null
              };
          } else {
              depth = {
                  discrete: false,
                  discreteValues: null,
                  continuousValues: depths
              }
          }

          dimensions = {
              height: height,
              width: width,
              depth: depth,
          };

          console.log(dimensions);

          var materials = [];
          for (let i = 0; i < this.materialList; i++){
              materials.push({
                  designation:{
                      nameContent: this.materialList[i].designation
                  },
                  finishingList: this.materialList[i].finishingList
              });
          };

          var productToCreate = {
              name: {
                  nameContent: document.getElementById('productName').value
              },
              category: {
                  id: 1
              },
              dimensions: dimensions,
              materials: materials,
              parts: []
          };
          this.props.createProduct(productToCreate);
      };

    const { activeItem } = this.state;

      var options = [];
      if (this.props.products.data != null) {
          for (let i = 0; i < this.props.products.data.length; ++i){
              let product = this.props.products.data[i];
              options.push({
                  key: product.id,
                  text: product.productName,
                  value: product.id
              });
          }
      }

      var materials = [];
      if (this.props.materials.data != null) {
          for (let i = 0; i < this.props.materials.data.length; ++i){
              let material = this.props.materials.data[i];
              materials.push({
                  key: material.id,
                  text: material.designation,
                  value: material
              });
          }
      }

      const Create = () => (
          <div class="Product-data">
              <Form>
                  <Form.Field>
                      <label>Product Name</label>
                      <input id='productName' placeholder='Product Name' />
                  </Form.Field>
                  <Form.Group widths='equal'>
                      <Form.Field>
                          <Dropdown placeholder='Select Category' fluid search selection options={options} />
                      </Form.Field>
                      <Form.Field>
                          <Dropdown placeholder='Select Materials' onChange={this.chooseMaterials}
                                    fluid multiple selection options={materials} />
                      </Form.Field>
                  </Form.Group>
                  <Form.Group widths='equal'>
                      <div class="Product-dimensions">
                          <Form.Group widths='equal'>
                              <Form.Field>
                                  <input id='widthProduct' placeholder='Width' />
                              </Form.Field>
                          </Form.Group>
                      </div>
                      <div class="Product-dimensions">
                          <Form.Group widths='equal'>
                              <Form.Field>
                                  <input id='heightProduct' placeholder='Height' />
                              </Form.Field>
                          </Form.Group>
                      </div>
                      <div class="Product-dimensions">
                          <Form.Group widths='equal'>
                              <Form.Field>
                                  <input id='depthProduct' placeholder='Depth' />
                              </Form.Field>
                          </Form.Group>
                      </div>
                  </Form.Group>
                  <Form.Group widths='equal'>
                  <Form.Field>
                          <Checkbox id='widthDiscrete' label='Discrete' />
                      </Form.Field>
                      <Form.Field>
                          <Checkbox id='heightDiscrete' label='Discrete' />
                      </Form.Field>
                      <Form.Field>
                          <Checkbox id='depthDiscrete' label='Discrete' />
                      </Form.Field>
                  </Form.Group>
                  <Form.Group widths='equal'>
                      <Form.Field>
                          <Dropdown placeholder='Select Parent' fluid search selection options={options} />
                      </Form.Field>
                      <Form.Field>
                          <Dropdown placeholder='Select Parts' fluid multiple selection options={options} />
                      </Form.Field>
                  </Form.Group>
                  <Form.Group widths='equal'>
                      <Form.Field>
                          <Checkbox label='Mandatory' />
                      </Form.Field>
                      <Form.Input
                          fluid
                          id='form-subcomponent-min-occupation'
                          label='Min Occupation'
                          placeholder='Min Occupation'
                      />
                      <Form.Input
                          fluid
                          id='form-subcomponent-max-occupation'
                          label='Max Occupation'
                          placeholder='Max Occupation'
                      />
                  </Form.Group>

                  <Button type='submit' onClick={onCreate} >Submit</Button>
              </Form>
          </div>
      );

    const Find = () => (
        <div class="Product-data">
            <Form>
                <div class="Product-dimensions">
                    <Form.Group widths='equal'>
                        <Dropdown id='dropdownProducts' onChange={this.chooseProduct}
                                  placeholder='Select Product' fluid search selection options={options} />
                        <Button type='submit' onClick={onFind}>Submit</Button>
                    </Form.Group>
                </div>
                <TextArea id='textAreaProducts' autoHeight readOnly placeholder='Product data'>{this.textArea}</TextArea>
            </Form>
        </div>
      );

      const Delete = () => (
          <div class="Product-data">
              <Form>
                  <Dropdown placeholder='Select Product' onChange={this.chooseProduct}
                            fluid search selection options={options} />
                  <p></p>
                  <Button onClick={onDelete} type='submit'>Delete</Button>
              </Form>
          </div>
      );

    let pages = {
        create: (
            <Container name='create'>
                <Create></Create>
            </Container>
        ),
            
        find: (
            <Container name='find'>
                <Find></Find>
            </Container>
        ),

        update: (
            <Container name='update'>
                <Update></Update>
            </Container>
        ),

        delete: (
            <Container name='delete'>
                <Delete></Delete>
            </Container>
        ),
       
       };
   
       let renderActivePage = () => pages[this.state.activeItem];

      var products;
      if (this.props.products.data != null) {

          products = this.props.products.data.map(product => (
              <div key={product.id} className="Product-presentation">
                  <div style={{marginTop: -22 + 'px'}}>
                      <img
                          src={img}
                          className="Product-img"/>
                      <List.Content>
                          <div className="Product-info">
                              <List.Header><b>{product.productName}</b></List.Header>
                              <div style={{marginTop: -5 + 'px'}}></div>
                          </div>
                      </List.Content>
                  </div>
              </div>
          ));

      }

    return (
      <div>
          <List position="left" class="ProductListContainer" celled>
              <h1><center>Products list</center></h1>
              {products}
        </List>

        <div class="Product-actions">
            <Menu attached='top' tabular>
            <Menu.Item name='create' 
                active={activeItem === 'create'} 
                onClick={this.handleItemClick} />
            <Menu.Item
                name='find'
                active={activeItem === 'find'}
                onClick={this.handleItemClick}
            />
            <Menu.Item
                name='update'
                active={activeItem === 'update'}
                onClick={this.handleItemClick}
            />
            <Menu.Item
                name='delete'
                active={activeItem === 'delete'}
                onClick={this.handleItemClick}
            />
            </Menu>
        </div>
        

        {pages[this.state.activeItem]}

      </div>
    )
  }
}

const mapStateToProps = state => ({
    products: state.products.products,
    productById: state.products.productById,
    productCreated: state.catalogCreate.product,
    productDeleted: state.catalogDelete.product,
    materials: state.products.materials
});

export default connect(mapStateToProps, { fetchProducts, fetchProductById, createProduct, deleteProduct, fetchMaterials })(Product);