using System.Collections.Generic;
using SIC.DTOs;

namespace SIC.Models
{
    public class Value : ValueObject
    {
        public long Id { get; set; }
        
        public double ValueDouble { get; set; }
        
        /**
        Converts an instance of Value to its DTO equivalent
         */
        public ValueDTO toDTO() {
            var value = new ValueDTO(){
                ValueDoubleDTO = ValueDouble
            };

            return value;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Id;
            yield return ValueDouble;
        }
    }
}