module.exports = {
    orderDTO : function(Order) {
    return {
        client: Order.client,
        items: Order.items,
        price: Order.price
      }
  }
}