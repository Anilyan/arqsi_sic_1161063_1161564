module.exports = {
    RequiredItemsRestriction : function(requiredItemsCounter, parentItems, requiredItemsList)
    {
        var parentItemsCounter = 0;

        requiredItemsList.forEach(product => {
            parentItems.forEach(item => {
                if(item.productId == product) {
                    parentItemsCounter++;
                }
            });
        });       

        if(requiredItemsCounter == parentItemsCounter){
            return true;
        } else{
            return false;
        }
    }
}