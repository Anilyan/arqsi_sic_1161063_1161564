using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIC.DAL;
using SIC.DTOs;
using SIC.Models;

namespace SIC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DimensionsController : ControllerBase
    {
        private readonly BusinessContext _context;

        public DimensionsController(BusinessContext context)
        {
            _context = context;
//            if(!_context.Dimensions.Any()){
//                var values = new List<Value> {new Value {ValueDouble = 20.0}};
//                var values = new List<Value> {new Value {ValueDouble = 20.0}};
//                var values = new List<Value> {new Value {ValueDouble = 20.0}};
//                Dimensions sampleDimensions = new Dimensions {
//                    Height = new Dimension { Discrete = true, DiscreteValues = values, ContinuousValues = null },
//                    Width = new Dimension { Discrete = true, DiscreteValues = values, ContinuousValues = null },
//                    Depth = new Dimension { Discrete = true, DiscreteValues = values, ContinuousValues = null }
//                };
//                _context.Dimensions.Add(sampleDimensions);
//                _context.SaveChanges();
//            }
        }

        // GET: api/Dimensions
        [HttpGet]
        public IEnumerable<DimensionsDTO> GetDimensions()
        {
            var dimensions = from d in _context.Dimensions select d.toDTO();
            return dimensions;
        }

        // GET: api/Dimensions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDimensions([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dimensions = await _context.Dimensions.FindAsync(id);

            if (dimensions == null)
            {
                return NotFound();
            }

            DimensionsDTO dimensionsDTO = dimensions.toDTO();
            return Ok(dimensionsDTO);
        }

        // PUT: api/Dimensions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDimensions([FromRoute] long id, [FromBody] Dimensions dimensions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            var dimensionsToChange = await _context.Dimensions.FindAsync(id);

            dimensionsToChange.Depth = dimensions.Depth;
            dimensionsToChange.Height = dimensions.Height;
            dimensionsToChange.Width = dimensions.Width;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DimensionsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Dimensions
        [HttpPost]
        public async Task<IActionResult> PostDimensions([FromBody] Dimensions dimensions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Dimensions.Add(dimensions);
            await _context.SaveChangesAsync();

            DimensionsDTO dimensionsDTO = dimensions.toDTO();
            return CreatedAtAction("GetDimensions", new { id = dimensions.Id }, dimensionsDTO);
        }

        // DELETE: api/Dimensions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDimensions([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dimensions = await _context.Dimensions.FindAsync(id);
            if (dimensions == null)
            {
                return NotFound();
            }

            _context.Dimensions.Remove(dimensions);
            await _context.SaveChangesAsync();

            DimensionsDTO dimensionsDTO = dimensions.toDTO();
            return Ok(dimensionsDTO);
        }

        private bool DimensionsExists(long id)
        {
            return _context.Dimensions.Any(e => e.Id == id);
        }
    }
}