import React, { Component } from 'react';
import { Input, Menu, Segment } from 'semantic-ui-react'
import { Container, Image, List } from 'semantic-ui-react'
import {Create, Delete, Find, Update} from './index'
import img from '../../../resources/product.png'

import {connect} from "react-redux";
import {fetchFinishing} from "../../../redux/actions/fetchActions";
import {createFinishing} from "../../../redux/actions/createActions";
import Form from "semantic-ui-react/dist/es/collections/Form/Form";
import Button from "semantic-ui-react/dist/es/elements/Button/Button";

class Finishing extends Component {
  state = { activeItem: 'create' };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

    componentDidMount() {
        this.props.fetchFinishing();
    }

  render() {

        var onCreate = () => {
            var finishingToCreate = {
                designation: {
                    nameContent: document.getElementById('finishingName').value
                }
            };
            this.props.createFinishing(finishingToCreate);
        };

    const { activeItem } = this.state;

      const Create = () => (
          <div class="Product-data">
              <Form>
                  <Form.Field>
                      <label>Finishing Name</label>
                      <input id='finishingName' placeholder='Finishing Name' />
                  </Form.Field>
                  <Button onClick={onCreate} type='submit'>Submit</Button>
              </Form>
          </div>
      );

    let pages = {
        create: (
            <Container name='create'>
                <Create></Create>
            </Container>
        ),

        update: (
            <Container name='find'>
                <Update></Update>
            </Container>
        ),

        delete: (
            <Container name='delete'>
                <Delete></Delete>
            </Container>
        ),
       
       };
   
       let renderActivePage = () => pages[this.state.activeItem];

      var finishing;
      if (this.props.products.data != null) {

          finishing = this.props.products.data.map(finishing => (
              <div key={finishing.id} class="Product-presentation">
                  <div style={{marginTop: -22 + 'px'}} >
                      <img src={img} class="Product-img"/>
                      <List.Content>
                          <div class="Product-info">
                              <List.Header><b>{finishing.designation}</b></List.Header>
                              <div style={{marginTop: -5 + 'px'}} ></div>
                          </div>
                      </List.Content>
                  </div>
              </div>
          ));
      }

    return (
      <div>
          <List position="left" class="ProductListContainer" celled>
              <h1><center>Finishing list</center></h1>
              {finishing}
        </List>

        <div class="Product-actions">
            <Menu attached='top' tabular>
            <Menu.Item name='create' 
                active={activeItem === 'create'} 
                onClick={this.handleItemClick} />
            <Menu.Item
                name='update'
                active={activeItem === 'update'}
                onClick={this.handleItemClick}
            />
            <Menu.Item
                name='delete'
                active={activeItem === 'delete'}
                onClick={this.handleItemClick}
            />
            </Menu>
        </div>
        

        {pages[this.state.activeItem]}

      </div>
    )
  }
}

const mapStateToProps = state => ({
    products: state.products.finishing,
    finishingCreated: state.catalogCreate.finishing
});

export default connect(mapStateToProps, { fetchFinishing, createFinishing })(Finishing);