using System.Collections.Generic;
using SIC.Models;
using SIC.DTOs;

namespace SIC.DTOs
{
    public class ProductDTO
    {
        public long Id { get; set; }
        
        public string ProductName { get; set; }

        public long CategoryId { get; set; }

        public DimensionsDTO Dimensions { get; set; }

        public IList<MaterialDTO> Materials { get; set; }

        public long? ParentProductId { get; set; }
        public bool? Mandatory { get; set; }
        public long? MaxOccupation { get; set; }
        public long? MinOccupation { get; set; }
        public IList<long> Parts { get; set; }
    }
}