import React from 'react';
import {List } from 'semantic-ui-react'

const ProductInstructions = () => (
    <div>
        <h1>You are in the catalog</h1>
        <p>Here is where you manage products to choose from in the future.</p>
        <p>If you want to create everything from scratch, we recommend the following order:</p>
        <p></p>
        <List as='ol'>
            <List.Item as='li'>Finishing</List.Item>
            <List.Item as='li'>Material</List.Item>
            <List.Item as='li'>Category</List.Item>
            <List.Item as='li'>Product</List.Item>
        </List>
    </div>
);

export default ProductInstructions;