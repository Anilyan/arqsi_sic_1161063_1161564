import React, { Component } from 'react';
import { Container, Menu, List } from 'semantic-ui-react'
import {Product} from './product'
import {Material} from './material'
import {Category} from './category'
import {Finishing} from './finishing'
import {ProductInstructions} from '../others'

export default class Catalog extends Component {
    state = { activeItem: 'home' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    let pages = {
        instructions: (
            <Container name='instructions'>
                <ProductInstructions></ProductInstructions>
            </Container>
        ),

        product: (
            <Container name='product'>
                <Product></Product>
            </Container>
        ),
            
        category: (
            <Container name='category'>
                <Category></Category>
            </Container>
        ),

        material: (
            <Container name='material'>
                <Material></Material>
            </Container>
        ),

        finishing: (
            <Container name='finishing'>
                <Finishing></Finishing>
            </Container>
        ),
       
       };

    return (
      <div>
        <Menu pointing>
          <Menu.Item
            name='instructions'
            icon="edit"
            active={activeItem === 'instructions'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='product'
            content="Manage Products"
            active={activeItem === 'product'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='category'
            content="Manage Categories"
            active={activeItem === 'category'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='material'
            content="Manage Materials"
            active={activeItem === 'material'}
            onClick={this.handleItemClick}
          />
          <Menu.Item
            name='finishing'
            content="Manage Finishings"
            active={activeItem === 'finishing'}
            onClick={this.handleItemClick}
          />
        </Menu>

       {pages[this.state.activeItem]}
      </div>
    )
  }
}
