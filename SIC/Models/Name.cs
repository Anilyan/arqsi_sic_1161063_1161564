using System.Collections.Generic;

namespace SIC.Models
{
    /* ValueObject */
    public class Name : ValueObject
    {
        public long Id { get; set;}
        public string NameContent { get; set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Id;
            yield return NameContent;
        }
    }
}