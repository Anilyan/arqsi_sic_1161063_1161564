namespace SIC.Models
{
    /* Concrete representation of the Dimensions of a ProductItem */
    public class ConcreteDimensions
    {
        public long Id { get; set; }
        
        public double Height { get; set; }
        public double Width { get; set; }
        public double Depth { get; set; }
    }
}