using System.Collections.Generic;

namespace SIC.DTOs
{
    public class MaterialDTO
    {
        public string Designation { get; set; }

        public List<FinishingDTO> FinishingList { get; set; }
    }
}