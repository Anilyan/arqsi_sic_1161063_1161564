using System;
using Xunit;
using SIC.Models;
using SIC.DTOs;
using System.Collections.Generic;

namespace Tests
{
    public class DimensionsTest
    {
        
        private Dimensions _dimensions;

        public DimensionsTest()
        {
            
        }
        
        [Fact]
        public void testToDto()
        {
            _dimensions = new Dimensions{Height = 50, Width = 80.2, Depth = 20};

            var _dimensionsDTO = _dimensions.toDTO();
            
            Assert.True(_dimensionsDTO.HeightDTO.Equals(50), "");
            Assert.True(_dimensionsDTO.WidthDTO.Equals(80.2), "");
            Assert.True(_dimensionsDTO.DepthDTO.Equals(20), "");
        }
    }
}
